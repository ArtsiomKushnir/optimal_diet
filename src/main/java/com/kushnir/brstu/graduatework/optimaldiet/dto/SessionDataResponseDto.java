package com.kushnir.brstu.graduatework.optimaldiet.dto;

import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SessionDataResponseDto {
    private User user;
    private Plan plan;
}
