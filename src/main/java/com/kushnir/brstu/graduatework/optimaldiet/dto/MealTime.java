package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalTime;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MealTime {
    private Long id;
    private Integer weekDirectionId;
    private Short dayOfWeek;
    private Long orderId;
    private LocalTime time;
    private String name;
    private Double rationVolume;
    private CalculationData calculation;
    private Set<Recipe> recipes;
}
