package com.kushnir.brstu.graduatework.optimaldiet.repository;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Unit;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UnitRepository extends PagingAndSortingRepository<Unit, Long> {
    List<Unit> findAllByIdIn(List<Long> ids);
}
