package com.kushnir.brstu.graduatework.optimaldiet.entity.embedded;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class Step {
    @Column(name = "step_id")
    private Long id;
    @Column(name = "step_description")
    private String description;
}
