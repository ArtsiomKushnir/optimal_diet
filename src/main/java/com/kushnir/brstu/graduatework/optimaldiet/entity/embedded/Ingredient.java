package com.kushnir.brstu.graduatework.optimaldiet.entity.embedded;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class Ingredient {
    private String name;
    private BigDecimal count;
    private String unit;
    private Double unitCoefficient;
    private BigDecimal calories;
    private BigDecimal fat;
    private BigDecimal protein;
    private BigDecimal carbohydrates;
}
