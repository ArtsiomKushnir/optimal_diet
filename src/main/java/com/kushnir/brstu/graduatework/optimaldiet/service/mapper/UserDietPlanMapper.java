package com.kushnir.brstu.graduatework.optimaldiet.service.mapper;

import com.kushnir.brstu.graduatework.optimaldiet.dto.*;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.UserDietPlan;
import com.kushnir.brstu.graduatework.optimaldiet.service.RecipeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.kushnir.brstu.graduatework.optimaldiet.entity.converter.RecipesIdListConverter.convertIds;
import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.defaultWeeks;
import static org.springframework.util.CollectionUtils.isEmpty;

@Component
@RequiredArgsConstructor
public class UserDietPlanMapper {

    private final RecipeService recipeService;

    public HashSet<UserDietPlan> map(final User user, final Plan plan) {
        final HashSet<UserDietPlan> userDietPlans = new HashSet<>();

        final HashMap<Integer, WeekPlan> weeks = plan.getWeeks();

        weeks.forEach((weekKey, weekPlan) -> {
            weekPlan.getDays().forEach((dayKey, dayPlan) -> {
                dayPlan.getMealTimes().forEach((mtk, mealTime) -> {

                    userDietPlans.add(UserDietPlan.builder()
                            .id(mealTime.getId())
                            .userId(user.getId())
                            .name(mealTime.getName())
                            .weekId(weekKey)
                            .dayOfWeek(dayPlan.getDayOfWeek())
                            .recipeIds(convertIds(mealTime.getRecipes()))
                            .scheduledTime(mealTime.getTime())
                            .orderId(mealTime.getOrderId())
                            .rationVolume(mealTime.getRationVolume())
                            .isCurrent(false)
                            .build());
                });
            });
        });

        return userDietPlans;
    }

    public void mapUserDietPlanIdToMealTimeId(final Plan plan, final List<UserDietPlan> savedUserDietPlans) {
        final HashMap<Integer, WeekPlan> weeks = plan.getWeeks();

        savedUserDietPlans.forEach(userDietPlan -> {

            final WeekPlan week = weeks.get(userDietPlan.getWeekId());
            final DayPlan day = week.getDays().get(userDietPlan.getDayOfWeek());
            final MealTime mealTime = day.getMealTimes().get(userDietPlan.getOrderId());

            mealTime.setId(userDietPlan.getId());

        });
    }

    public void mapToPlan(final Set<UserDietPlan> userDietPlanSet, final Plan plan) {
        if (isEmpty(userDietPlanSet)) {
            plan.setWeeks(defaultWeeks());
            return;
        }

        HashMap<Integer, WeekPlan> weeks = new HashMap<>();

        userDietPlanSet.forEach(userDietPlan -> {
            final var weekId = userDietPlan.getWeekId();
            final var dayOfWeek = userDietPlan.getDayOfWeek();
            final var orderId = userDietPlan.getOrderId();
            final var id = userDietPlan.getId();
            final var name = userDietPlan.getName();
            final var time = userDietPlan.getScheduledTime();
            final var rationVolume = userDietPlan.getRationVolume();
            final var recipeIds = userDietPlan.getRecipeIds();

            var week = weeks.get(weekId);

            if (week != null) {
                var days = week.getDays();
                var day = days.get(dayOfWeek);

                if (day != null) {
                    var mealTimes =  day.getMealTimes();
                    var mealTime = mealTimes.get(orderId);

                    if (mealTime != null) {
                        // TODO problem
                        System.out.println("mealTime: \n" + mealTime);
                    } else {
                        mealTimes.put(orderId, buildMealTime(id, weekId, dayOfWeek, name, orderId, time, rationVolume, recipeIds));
                    }

                } else {
                    days.put(dayOfWeek, DayPlan.builder()
                            .dayOfWeek(dayOfWeek)
                            .mealTimes(new HashMap<>() {{
                                put(orderId, buildMealTime(id, weekId, dayOfWeek, name, orderId, time, rationVolume, recipeIds));
                            }})
                            .build());
                }

            } else {
                week = WeekPlan.builder()
                        .days(new TreeMap<>() {{
                            put(dayOfWeek, DayPlan.builder()
                                    .dayOfWeek(dayOfWeek)
                                    .mealTimes(new HashMap<>() {{
                                        put(orderId, buildMealTime(id, weekId, dayOfWeek, name, orderId, time, rationVolume, recipeIds));
                                    }})
                                    .build());
                        }})
                        .directionId(weekId)
                        .build();

                weeks.put(weekId, week);
            }

        });

        plan.setWeeks(weeks);
    }

    private Set<Recipe> buildRecipes(final ArrayList<Long> recipeIds) {
        HashSet<Recipe> recipeHashSet = new HashSet<>();

        if (isEmpty(recipeIds)) return recipeHashSet;

        final List<com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe> recipes = recipeService.findAllByIdIn(recipeIds);

        return recipes.stream().map(recipe -> Recipe.builder()
                        .id(recipe.getId())
                        .name(recipe.getName())
                        .calculation(CalculationData.builder()
                                .calories(0L)
                                .nutrients(Nutrients.builder()
                                        .protein(0D)
                                        .carbohydrates(0D)
                                        .fats(0D)
                                        .build())
                                .build())
                        .originalRecipe(recipe)
                .build())
                .collect(Collectors.toSet());
    }

    private MealTime buildMealTime(final Long id,
                                   final Integer weekId,
                                   final Short dayOfWeek,
                                   final String name,
                                   final Long orderId,
                                   final LocalTime time,
                                   final Double rationVolume,
                                   final ArrayList<Long> recipeIds) {
        return MealTime.builder()
                .id(id)
                .weekDirectionId(weekId)
                .dayOfWeek(dayOfWeek)
                .name(name)
                .orderId(orderId)
                .time(time)
                .rationVolume(rationVolume)
                .recipes(buildRecipes(recipeIds))
                .build();
    }
}
