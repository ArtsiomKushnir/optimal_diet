package com.kushnir.brstu.graduatework.optimaldiet.entity;

import com.kushnir.brstu.graduatework.optimaldiet.entity.converter.RecipesIdListConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;
import java.util.ArrayList;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "user_diet_plan")
public class UserDietPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String name;
    private Long userId;
    private Integer weekId;
    private Short dayOfWeek;
    private LocalTime scheduledTime;
    private Long orderId;
    private Double rationVolume;
    private Boolean isCurrent;

    @Convert(converter = RecipesIdListConverter.class)
    private ArrayList<Long> recipeIds;
}
