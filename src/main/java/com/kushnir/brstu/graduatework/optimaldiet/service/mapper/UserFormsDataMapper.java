package com.kushnir.brstu.graduatework.optimaldiet.service.mapper;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.UserFormsData;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.defaultFormsData;

public class UserFormsDataMapper {

    public static void map(final User user, final Plan plan) {
        final UserFormsData userFormsData = user.getFormsData();

        if (userFormsData != null) {
            plan.getFormsData().getPersonData().setAge(userFormsData.getAge());
            plan.getFormsData().getPersonData().setHeight(userFormsData.getHeight());
            plan.getFormsData().getPersonData().setWeight(userFormsData.getWeight());
            plan.getFormsData().getPersonData().setGender(userFormsData.getGender());
            plan.getFormsData().getPersonData().setActivityLevel(userFormsData.getActivityLevel());

            plan.getFormsData().setDietAim(userFormsData.getDietAim());
            plan.getFormsData().getNutrientsAim().setFatCoefficient(userFormsData.getSelectedFatCoefficient());
            plan.getFormsData().getNutrientsAim().setProteinCoefficient(userFormsData.getSelectedProteinCoefficient());
        } else {
            plan.setFormsData(defaultFormsData());
        }
    }

    public static UserFormsData map(final Plan plan) {
        if (plan == null || plan.getFormsData() == null) return null;

        final var formsData = plan.getFormsData();

        final UserFormsData.UserFormsDataBuilder builder = UserFormsData.builder();

        final var personData = formsData.getPersonData();

        // stage one data
        if (personData != null) {
            builder.age(personData.getAge());
            builder.gender(personData.getGender());
            builder.height(personData.getHeight());
            builder.weight(personData.getWeight());
            builder.activityLevel(personData.getActivityLevel());
        }

        // stage two data
        if (formsData.getDietAim() != null) {
            builder.dietAim(formsData.getDietAim());
        }

        if (formsData.getNutrientsAim() != null) {
            builder.selectedProteinCoefficient(formsData.getNutrientsAim().getProteinCoefficient());
            builder.selectedFatCoefficient(formsData.getNutrientsAim().getFatCoefficient());
        }

        return builder.build();
    }
}
