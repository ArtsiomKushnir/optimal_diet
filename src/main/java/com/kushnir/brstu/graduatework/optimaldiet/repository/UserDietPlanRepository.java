package com.kushnir.brstu.graduatework.optimaldiet.repository;

import com.kushnir.brstu.graduatework.optimaldiet.entity.UserDietPlan;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface UserDietPlanRepository extends JpaRepository<UserDietPlan, Long> {
    Set<UserDietPlan> findAllByUserId(final Long userId);
}
