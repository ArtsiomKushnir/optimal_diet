package com.kushnir.brstu.graduatework.optimaldiet.repository;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Ingredient;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IngredientRepository extends PagingAndSortingRepository<Ingredient, Long>, JpaSpecificationExecutor<Ingredient> {
    boolean existsByName(final String name);
}
