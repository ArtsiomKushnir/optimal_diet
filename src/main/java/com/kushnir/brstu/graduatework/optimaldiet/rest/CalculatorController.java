package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.service.CalculatorService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserDietPlanService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.PLAN_KEY;
import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.USER_KEY;

@RestController
@RequestMapping("/api/calculator")
@RequiredArgsConstructor
public class CalculatorController {

    private final CalculatorService calculatorService;
    private final UserService userService;
    private final UserDietPlanService userDietPlanService;

    @GetMapping
    public Plan getPlan(@SessionAttribute(PLAN_KEY) Plan sessionPlan) {
        return sessionPlan;
    }

    @PostMapping
    public Plan calculate(@RequestBody final Plan plan,
                          @SessionAttribute(USER_KEY) User user,
                          @SessionAttribute(PLAN_KEY) Plan sessionPlan) {
        calculatorService.calculate(plan, sessionPlan);
        if(user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }
        return sessionPlan;
    }
}
