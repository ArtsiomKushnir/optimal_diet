package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CalculationResults {
    private CalculationData base;
    private CalculationData corrected;
    private CalculationData actualTotal;
    private boolean isOutOfAim;
}
