package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Unit;
import com.kushnir.brstu.graduatework.optimaldiet.repository.UnitRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Arrays.asList;

@Service
@RequiredArgsConstructor
public class UnitService {

    private final UnitRepository unitRepository;

    public List<Unit> getAll() {
        return unitRepository.findAllByIdIn(asList(1L, 2L, 3L));
    }
}
