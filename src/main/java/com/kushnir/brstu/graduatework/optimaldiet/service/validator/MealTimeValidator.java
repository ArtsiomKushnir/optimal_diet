package com.kushnir.brstu.graduatework.optimaldiet.service.validator;

import com.kushnir.brstu.graduatework.optimaldiet.dto.MealTime;
import com.kushnir.brstu.graduatework.optimaldiet.exception.MealTimeValidationException;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.hibernate.internal.util.StringHelper.isBlank;

@Component
public class MealTimeValidator {

    public void validate(final MealTime mealTime) throws MealTimeValidationException {
        final Map<String, String> errors = new HashMap<>();

        if (isBlank(mealTime.getName())) errors.put("name", "Укажите названия для времени приема пищи");

        if (mealTime.getTime() == null) errors.put("time", "Укажите время приема пищи");


        if (!errors.isEmpty()) throw MealTimeValidationException.builder()
                .errors(errors)
                .build();
    }
}
