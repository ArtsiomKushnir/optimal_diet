package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.dto.SessionDataResponseDto;
import com.kushnir.brstu.graduatework.optimaldiet.dto.UserDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserLoginException;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserRegistrationException;
import com.kushnir.brstu.graduatework.optimaldiet.service.CalculatorService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserDietPlanService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.*;

@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final CalculatorService calculatorService;
    private final UserDietPlanService userDietPlanService;

    @GetMapping
    public User getUser(@SessionAttribute(USER_KEY) final User user) {
        return user;
    }

    @PostMapping("/register")
    public User register(@RequestBody final UserDto userDto,
                         @SessionAttribute(PLAN_KEY) final Plan sessionPlan,
                         final HttpSession httpSession) throws UserRegistrationException {
        final User user = userService.register(userDto, sessionPlan);
        httpSession.setAttribute(USER_KEY, user);

        calculatorService.calculate(sessionPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return user;
    }

    @PostMapping("/login")
    public SessionDataResponseDto login(@RequestBody final UserDto userDto,
                                        @SessionAttribute(PLAN_KEY) final Plan sessionPlan,
                                        final HttpSession httpSession) throws UserLoginException {
        final User user = userService.login(userDto, sessionPlan);
        httpSession.setAttribute(USER_KEY, user);

        calculatorService.calculate(sessionPlan);

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    @PostMapping("/logout")
    public SessionDataResponseDto logout(@SessionAttribute(USER_KEY) User user,
                                         @SessionAttribute(PLAN_KEY) Plan sessionPlan,
                                         final HttpSession httpSession) {
        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);

            final User emptyUser = defaultUser();
            final Plan emptySessionPlan = defaultPlan();
            httpSession.setAttribute(USER_KEY, emptyUser);
            httpSession.setAttribute(PLAN_KEY, emptySessionPlan);

            return SessionDataResponseDto.builder()
                    .user(emptyUser)
                    .plan(emptySessionPlan)
                    .build();
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }
}
