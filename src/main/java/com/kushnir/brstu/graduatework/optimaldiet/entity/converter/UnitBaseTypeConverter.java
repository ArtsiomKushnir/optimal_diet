package com.kushnir.brstu.graduatework.optimaldiet.entity.converter;

import com.kushnir.brstu.graduatework.optimaldiet.entity.UnitBaseType;

import javax.persistence.AttributeConverter;

public class UnitBaseTypeConverter implements AttributeConverter<UnitBaseType, Integer> {
    @Override
    public Integer convertToDatabaseColumn(final UnitBaseType attribute) {
        return attribute.getId();
    }

    @Override
    public UnitBaseType convertToEntityAttribute(final Integer dbData) {
        for (UnitBaseType type : UnitBaseType.values()) {
            if (type.getId().equals(dbData)) return type;
        }
        return null;
    }
}
