package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.RecipeDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.embedded.Ingredient;
import com.kushnir.brstu.graduatework.optimaldiet.entity.embedded.Step;
import com.kushnir.brstu.graduatework.optimaldiet.exception.InvalidRecipeDtoRequestException;
import com.kushnir.brstu.graduatework.optimaldiet.service.RecipeCalculationService;
import com.kushnir.brstu.graduatework.optimaldiet.service.RecipeService;
import com.kushnir.brstu.graduatework.optimaldiet.service.specification.recipe.RecipeSearchCriteria;
import com.kushnir.brstu.graduatework.optimaldiet.service.validator.RecipeDtoValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.USER_KEY;
import static org.springframework.data.domain.Sort.Direction.ASC;

@RestController
@RequestMapping("/api/recipes")
@RequiredArgsConstructor
public class RecipeController {
    private final RecipeService recipeService;
    private final RecipeDtoValidator recipeDtoValidator;
    private final RecipeCalculationService recipeCalculationService;

    @GetMapping
    public Page<Recipe> findAll(
            @RequestParam(required = false) final Integer categoryId,
            @RequestParam(required = false) final Integer dishId,
            @RequestParam(required = false) final String name,
            @RequestParam(required = false) final Boolean onlyUserRecipes,
            @SortDefault(sort = "name", direction = ASC) final Pageable pageable,
            @SessionAttribute(USER_KEY) final User user) {

        final RecipeSearchCriteria recipeSearchCriteria = RecipeSearchCriteria.builder()
                .name(name)
                .dishId(dishId)
                .categoryId(categoryId)
                .build();

        if (onlyUserRecipes != null && onlyUserRecipes) {
            recipeSearchCriteria.setUserId(user.getId());
        }

        return recipeService.findAll(pageable, recipeSearchCriteria);
    }

    @GetMapping("/{id}")
    public Recipe findById(@PathVariable final Long id) {
        final Recipe recipe = recipeService.findById(id).orElseThrow();

        recipeCalculationService.calculate(recipe);

        return recipe;
    }

    @PostMapping
    public Recipe create(@RequestParam(required = false) final MultipartFile file,
                         final RecipeDto recipeDto,
                         @RequestPart final List<Ingredient> ingredients,
                         @RequestPart final List<Step> steps) throws IOException, InvalidRecipeDtoRequestException {
        recipeDtoValidator.validate(recipeDto, ingredients, steps);
        return recipeService.create(recipeDto, ingredients, steps, file);
    }

    @PutMapping
    public Recipe updateRecipe(@RequestParam(required = false) final MultipartFile file,
                               final RecipeDto recipeDto,
                               @RequestPart final List<Ingredient> ingredients,
                               @RequestPart final List<Step> steps,
                               @SessionAttribute(USER_KEY) final User user) throws InvalidRecipeDtoRequestException, IOException {
        recipeDtoValidator.validate(recipeDto, ingredients, steps);
        return recipeService.updateRecipe(recipeDto, user, ingredients, steps, file);
    }

    @DeleteMapping("/{id}")
    public void deleteRecipe(@PathVariable final Long id) {
        recipeService.deleteRecipe(id);
    }
}
