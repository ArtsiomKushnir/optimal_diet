package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.*;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpSession;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeMap;

@ControllerAdvice
public class SessionController {

    public static final String USER_KEY = "user";
    public static final String PLAN_KEY = "plan";

    @ModelAttribute(USER_KEY)
    public User setUser(final HttpSession httpSession) {
        httpSession.setMaxInactiveInterval(999999999);
        User user = (User) httpSession.getAttribute(USER_KEY);
        if (user == null) {
            user = defaultUser();
            httpSession.setAttribute(USER_KEY, user);
        }
        return user;
    }

    @ModelAttribute(PLAN_KEY)
    public Plan setPlan(final HttpSession httpSession) {
        Plan plan = (Plan) httpSession.getAttribute(PLAN_KEY);
        if (plan == null) {
            plan = defaultPlan();
            httpSession.setAttribute(PLAN_KEY, plan);
        }
        return plan;
    }

    public static User defaultUser() {
        return User.builder().build();
    }

    public static Plan defaultPlan() {
        return Plan.builder()
                .calculationResults(defaultCalculationResults())
                .formsData(defaultFormsData())
                .weeks(defaultWeeks())
                .build();
    }

    public static CalculationResults defaultCalculationResults() {
        return CalculationResults.builder()
                .base(CalculationData.builder()
                        .nutrients(Nutrients.builder().build())
                        .build())
                .corrected(CalculationData.builder()
                        .nutrients(Nutrients.builder().build())
                        .build())
                .actualTotal(CalculationData.builder()
                        .nutrients(Nutrients.builder().build())
                        .build())
                .build();
    }

    public static FormsData defaultFormsData() {
        return FormsData.builder()
                .personData(PersonData.builder().build())
                .nutrientsAim(NutrientsCoefficient.builder()
                        .proteinCoefficient(1.75)
                        .fatCoefficient(0.9)
                        .build())
                .build();
    }

    public static HashMap<Integer, WeekPlan> defaultWeeks() {
        return new HashMap<>() {{
            put(1, WeekPlan.builder()
                    .directionId(1)
                    .days(defaultDays(1))
                    .build());
        }};
    }

    public static TreeMap<Short, DayPlan> defaultDays(final Integer weekDirectionId) {
        return new TreeMap<>() {{
            put((short) 1, DayPlan.builder().dayOfWeek((short) 1)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 1, weekDirectionId))
                    .build());
            put((short) 2, DayPlan.builder().dayOfWeek((short) 2)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 2, weekDirectionId))
                    .build());
            put((short) 3, DayPlan.builder().dayOfWeek((short) 3)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 3, weekDirectionId))
                    .build());
            put((short) 4, DayPlan.builder().dayOfWeek((short) 4)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 4, weekDirectionId))
                    .build());
            put((short) 5, DayPlan.builder().dayOfWeek((short) 5)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 5, weekDirectionId))
                    .build());
            put((short) 6, DayPlan.builder().dayOfWeek((short) 6)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 6, weekDirectionId))
                    .build());
            put((short) 7, DayPlan.builder().dayOfWeek((short) 7)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .mealTimes(defaultMealTimes((short) 7, weekDirectionId))
                    .build());
        }};
    }

    private static HashMap<Long, MealTime> defaultMealTimes(final short dayOfWeek, final Integer weekDirectionId) {

        return new HashMap<>() {{
            var order = 1L;
            put(order, MealTime.builder()
                    .dayOfWeek(dayOfWeek)
                    .weekDirectionId(weekDirectionId)
                    .orderId(order)
                    .name("Завтрак")
                    .time(LocalTime.parse("07:30"))
                    .rationVolume(40.0)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .recipes(new HashSet<>())
                    .build());

            order ++;

            put(order, MealTime.builder()
                    .dayOfWeek(dayOfWeek)
                    .weekDirectionId(weekDirectionId)
                    .orderId(order)
                    .name("Обед")
                    .time(LocalTime.parse("12:30"))
                    .rationVolume(35.0)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .recipes(new HashSet<>())
                    .build());

            order ++;

            put(order, MealTime.builder()
                    .dayOfWeek(dayOfWeek)
                    .weekDirectionId(weekDirectionId)
                    .orderId(order)
                    .name("Ужин")
                    .time(LocalTime.parse("17:30"))
                    .rationVolume(25.0)
                    .calculation(CalculationData.builder()
                            .nutrients(Nutrients.builder().build())
                            .build())
                    .recipes(new HashSet<>())
                    .build());
        }};
    }

}
