package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.TreeMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WeekPlan {
    private Integer directionId;
    private TreeMap<Short, DayPlan> days;
    private boolean isOutOfAim;
}
