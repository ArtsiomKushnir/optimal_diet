package com.kushnir.brstu.graduatework.optimaldiet.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kushnir.brstu.graduatework.optimaldiet.entity.converter.GroupsConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "users")
@SecondaryTable(name="user_forms_data",
        pkJoinColumns=@PrimaryKeyJoinColumn(name="user_id"))
public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String email;

    @JsonIgnore
    private String password;

    @Convert(converter = GroupsConverter.class)
    private Set<Group> groups;

    @JsonIgnore
    private Boolean enabled;

    @JsonIgnore
    @Embedded
    private UserFormsData formsData;
}
