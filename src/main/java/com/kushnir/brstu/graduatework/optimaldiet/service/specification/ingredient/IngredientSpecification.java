package com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Ingredient;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

import static com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient.IngredientSpecificationBuilder.aPredicates;

public class IngredientSpecification {
    public static Specification<Ingredient> buildSpecification(final IngredientSearchCriteria ingredientSearchCriteria) {
        return (root, query, cb) -> {

            if (ingredientSearchCriteria == null) {
                return null;
            }

            final List<Predicate> predicates = aPredicates(root, cb, query)
                    .withName(ingredientSearchCriteria.getName())
                    .build();

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
