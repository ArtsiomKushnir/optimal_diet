package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.UserDietPlan;
import com.kushnir.brstu.graduatework.optimaldiet.repository.UserDietPlanRepository;
import com.kushnir.brstu.graduatework.optimaldiet.service.mapper.UserDietPlanMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserDietPlanService {

    private final UserDietPlanRepository userDietPlanRepository;
    private final UserDietPlanMapper userDietPlanMapper;

    public void saveDietPlan(final User user, final Plan plan) {
        final HashSet<UserDietPlan> userDietPlans = userDietPlanMapper.map(user, plan);

        final List<UserDietPlan> savedUserDietPlansList = userDietPlanRepository.saveAll(userDietPlans);

        userDietPlanMapper.mapUserDietPlanIdToMealTimeId(plan, savedUserDietPlansList);
    }

    public void mapUserDietPlan(final User user, final Plan plan) {
        final Set<UserDietPlan> userDietPlanSet = userDietPlanRepository.findAllByUserId(user.getId());

        userDietPlanMapper.mapToPlan(userDietPlanSet, plan);
    }

}
