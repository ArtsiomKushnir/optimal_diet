package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.*;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.embedded.Ingredient;
import com.kushnir.brstu.graduatework.optimaldiet.service.CalculatorService;
import com.kushnir.brstu.graduatework.optimaldiet.service.RecipeService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserDietPlanService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.*;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@RestController
@RequestMapping("/api/plan")
@RequiredArgsConstructor
public class PlanController {

    private final RecipeService recipeService;
    private final CalculatorService calculatorService;
    private final UserService userService;
    private final UserDietPlanService userDietPlanService;

    @DeleteMapping("/mealTime/{mealTimeOrder}/recipe/{recipeId}")
    public SessionDataResponseDto deleteRecipeFromMealTime(@PathVariable final Long mealTimeOrder,
                                                           @PathVariable final Long recipeId,
                                                           @RequestParam final Integer weekDirectionId,
                                                           @RequestParam final Short dayOfWeek,
                                                           @SessionAttribute(USER_KEY) User user,
                                                           @SessionAttribute(PLAN_KEY) Plan sessionPlan) {

        final MealTime sessionMealTime = sessionPlan.getWeeks().get(weekDirectionId).getDays().get(dayOfWeek).getMealTimes().get(mealTimeOrder);

        sessionMealTime.getRecipes().removeIf(recipe -> recipe.getId().equals(recipeId));

        calculatorService.calculate(sessionPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    @PostMapping("/mealTime/recipe/{recipeId}")
    public SessionDataResponseDto addRecipeToMealTime(@RequestBody final MealTime mealTime,
                                                      @PathVariable final Long recipeId,
                                                      @SessionAttribute(USER_KEY) User user,
                                                      @SessionAttribute(PLAN_KEY) Plan sessionPlan) {
        final Optional<Recipe> recipeOptional = recipeService.findById(recipeId);

        if (recipeOptional.isPresent()) {
            final WeekPlan weekPlan = sessionPlan.getWeeks().get(mealTime.getWeekDirectionId());
            final DayPlan dayPlan = weekPlan.getDays().get(mealTime.getDayOfWeek());

            final MealTime sessionMealTime = dayPlan.getMealTimes().get(mealTime.getOrderId());

            final Recipe recipe = recipeOptional.get();

            final List<Ingredient> ingredients = recipe.getIngredients();

            Double totalCalories = 0.0;
            double totalFat = 0.0;
            double totalProtein = 0.0;
            double totalCarbohydrates = 0.0;

            if (isNotEmpty(ingredients)) {
                for (Ingredient ingredient : ingredients) {
                    Double unitCoefficient = ingredient.getUnitCoefficient();
                    BigDecimal count = ingredient.getCount();

                    BigDecimal calories = ingredient.getCalories();
                    BigDecimal fat = ingredient.getFat();
                    BigDecimal protein = ingredient.getProtein();
                    BigDecimal carbohydrates = ingredient.getCarbohydrates();

                    totalCalories += ((calories.doubleValue() * unitCoefficient) * count.doubleValue());
                    totalFat += ((fat.doubleValue() * unitCoefficient) * count.doubleValue());
                    totalProtein += ((protein.doubleValue() * unitCoefficient) * count.doubleValue());
                    totalCarbohydrates += ((carbohydrates.doubleValue() * unitCoefficient) * count.doubleValue());
                }
            }

            com.kushnir.brstu.graduatework.optimaldiet.dto.Recipe recipeDto = com.kushnir.brstu.graduatework.optimaldiet.dto.Recipe.builder()
                    .id(recipe.getId())
                    .name(recipe.getName())
                    .originalRecipe(recipe)
                    .calculation(CalculationData.builder()
                            .calories(totalCalories.longValue() / recipe.getNumberOfServings())
                            .nutrients(Nutrients.builder()
                                    .protein(totalProtein / recipe.getNumberOfServings())
                                    .fats(totalFat / recipe.getNumberOfServings())
                                    .carbohydrates(totalCarbohydrates / recipe.getNumberOfServings())
                                    .build())
                            .build())
                    .build();

            sessionMealTime.getRecipes().add(recipeDto);
        }

        calculatorService.calculate(sessionPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    @PostMapping("/week")
    public SessionDataResponseDto addWeek(@SessionAttribute(USER_KEY) User user,
                                          @SessionAttribute(PLAN_KEY) Plan sessionPlan) {

        final Integer directionId = generateDirectionId(sessionPlan.getWeeks());

        final WeekPlan weekPlan = WeekPlan.builder()
                .directionId(directionId)
                .days(defaultDays(directionId))
                .build();

        sessionPlan.getWeeks().put(directionId, weekPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    @DeleteMapping("/week")
    public SessionDataResponseDto deleteWeek(@SessionAttribute(USER_KEY) User user,
                                             @SessionAttribute(PLAN_KEY) Plan sessionPlan,
                                             @RequestParam final Integer directionId) {

        sessionPlan.getWeeks().remove(directionId);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    private Integer generateDirectionId(final HashMap<Integer, WeekPlan> weekPlanList) {

        int max = weekPlanList.keySet().stream()
                .max(Integer::compareTo).orElse(0);

        int size = weekPlanList.size();

        if (max > size) {
            for (int i = 1; i <= size; i++) {
                if (!weekPlanList.containsKey(i)) {
                    return i;
                }
            }
        }

        return max + 1;
    }

}
