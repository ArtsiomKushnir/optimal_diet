package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NutrientsCoefficient {
    private double proteinCoefficient;
    private double fatCoefficient;
    private double carbohydratesCoefficient;
}
