package com.kushnir.brstu.graduatework.optimaldiet.service.validator;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PlanValidator {

    private final FormsDataValidator formsDataValidator;

    public void validate(final Plan plan) {
        formsDataValidator.validate(plan.getFormsData());
    }
}
