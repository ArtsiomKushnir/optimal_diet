package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.DayPlan;
import com.kushnir.brstu.graduatework.optimaldiet.dto.MealTime;
import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.dto.WeekPlan;
import com.kushnir.brstu.graduatework.optimaldiet.exception.MealTimeValidationException;
import com.kushnir.brstu.graduatework.optimaldiet.service.validator.MealTimeValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;

@Service
@RequiredArgsConstructor
public class MealTimeService {

    private final MealTimeValidator mealTimeValidator;
    private final CalculatorService calculatorService;

    public void addMealTime(final MealTime mealTime, final Plan sessionPlan) throws MealTimeValidationException {
        mealTimeValidator.validate(mealTime);

        mealTime.setRecipes(new HashSet<>());

        final WeekPlan weekPlan = sessionPlan.getWeeks().get(mealTime.getWeekDirectionId());
        final DayPlan dayPlan = weekPlan.getDays().get(mealTime.getDayOfWeek());

        HashMap<Long, MealTime> mealTimes = dayPlan.getMealTimes();

        if (mealTimes.size() > 7) return;

        var order = 1L;
        for (; order <= mealTimes.size(); order++) {
            if (mealTime.getTime().isBefore(mealTimes.get(order).getTime())) {
                mealTime.setOrderId(order);
                break;
            }
        }
        var itemsCountAfter = mealTimes.size() - order;
        if (itemsCountAfter == 0 && mealTime.getOrderId() == null) {
            mealTime.setOrderId(order + 1);
            mealTimes.put(mealTime.getOrderId(), mealTime);
        } else if (itemsCountAfter == 0 && mealTime.getOrderId() != null) {
            var lastMealTime = mealTimes.get((long) mealTimes.size());
            lastMealTime.setOrderId(order + 1);
            mealTimes.put(lastMealTime.getOrderId(), lastMealTime);
            mealTimes.put(order, mealTime);
        } else {
            long curr = mealTimes.size();
            for (long i = itemsCountAfter; i >= 0; i--) {
                var currMealTime = mealTimes.get(curr);
                long orderId = curr + 1;
                currMealTime.setOrderId(orderId);
                mealTimes.put(orderId, currMealTime);
                curr--;
            }
            mealTime.setOrderId(order);
            mealTimes.put(order, mealTime);
        }

        calculatorService.calculate(sessionPlan);

    }

    public void deleteMealTime(final Short dayOfWeek,
                               final Integer weekDirectionId,
                               final Long orderId,
                               final Plan sessionPlan) {

        var mealTimes = sessionPlan.getWeeks().get(weekDirectionId).getDays().get(dayOfWeek).getMealTimes();
        final MealTime mealTime = mealTimes.get(orderId);

        if (mealTime.getName().equals("Завтрак") || mealTime.getName().equals("Обед") || mealTime.getName().equals("Ужин"))
            return;

        var itemsCountAfter = mealTimes.size() - orderId;

        mealTimes.remove(orderId);
        if (itemsCountAfter != 0) {

            final HashMap<Long, MealTime> newMealTimes = new HashMap<>();

            long order = 1L;
            for (MealTime m : mealTimes.values()) {
                m.setOrderId(order);
                newMealTimes.put(order, m);
                order++;
            }
            sessionPlan.getWeeks().get(weekDirectionId).getDays().get(dayOfWeek).setMealTimes(newMealTimes);
        }

        calculatorService.calculate(sessionPlan);
    }
}
