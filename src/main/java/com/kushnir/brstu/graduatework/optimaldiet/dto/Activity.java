package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.*;

@Getter
@AllArgsConstructor
public enum Activity {
    LEVEL_1(1.2),
    LEVEL_2(1.375),
    LEVEL_3(1.550),
    LEVEL_4(1.725),
    LEVEL_5(1.9);

    private Double index;
}
