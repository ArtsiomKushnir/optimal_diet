package com.kushnir.brstu.graduatework.optimaldiet.entity;

import com.kushnir.brstu.graduatework.optimaldiet.entity.converter.UnitBaseTypeConverter;
import com.kushnir.brstu.graduatework.optimaldiet.entity.converter.UnitGroupsConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "units")
public class Unit {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private String name;
    private String shortName;

    @Convert(converter = UnitBaseTypeConverter.class)
    private UnitBaseType type;
    private Double coefficient;

    @Convert(converter = UnitGroupsConverter.class)
    private Set<String> groups;
}
