package com.kushnir.brstu.graduatework.optimaldiet.service.validator;

import com.kushnir.brstu.graduatework.optimaldiet.dto.UserDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserLoginException;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserRegistrationException;
import com.kushnir.brstu.graduatework.optimaldiet.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.kushnir.brstu.graduatework.optimaldiet.service.util.Encoder.encoding;
import static org.apache.logging.log4j.util.Strings.isBlank;

@Component
@RequiredArgsConstructor
public class UserValidator {
    private final UserRepository userRepository;

    public void validateRegistration(final UserDto userDto) throws UserRegistrationException {
        final Map<String, String> errors = new HashMap<>();

        if (isBlank(userDto.getEmail())) errors.put("email", "Адрес электронной почты не должен быть пустым");
        else if (!validateEmail(userDto.getEmail())) errors.put("email", "Введен некорректный адрес электронной почты");
        else if (userRepository.existsByEmail(userDto.getEmail()))
            errors.put("email", "Пользователь c таким email уже присутствует");
        if (isBlank(userDto.getPassword())) errors.put("password", "Введите пароль");
        else if (userDto.getPassword().length() < 5)
            errors.put("password", "Длина пароля не должна быть меньше 6 символов");
        else if (!userDto.getPassword().equals(userDto.getConfirmPassword()))
            errors.put("confirmPassword", "Пароли не совпадают");

        if (!errors.isEmpty()) throw UserRegistrationException.builder()
                .errors(errors)
                .build();
    }

    public User validateLoginAndGet(final UserDto userDto) throws UserLoginException {
        final Map<String, String> errors = new HashMap<>();
        User user = null;

        if (isBlank(userDto.getEmail())) {
            errors.put("email", "Адрес электронной почты не должен быть пустым");
        } else {
            Optional<User> userOptional = userRepository.findUserByEmail(userDto.getEmail());
            if (userOptional.isEmpty()) errors.put("email", "Пользователь не найден");
            else if (isBlank(userDto.getPassword())) {
                errors.put("password", "Введите пароль");
            } else {
                user = userOptional.get();
                final String encodedPassword = encoding(userDto.getPassword());
                final String userPassword = user.getPassword();

                if (!encodedPassword.equals(userPassword)) {
                    errors.put("password", "Неверный пароль");
                }
            }
        }

        if (!errors.isEmpty()) throw UserLoginException.builder()
                .errors(errors)
                .build();
        return user;
    }

    private boolean validateEmail(String email) {
        try {
            InternetAddress internetAddress = new InternetAddress(email);
            internetAddress.validate();
            return true;
        } catch (AddressException e) {
            return false;
        }
    }
}
