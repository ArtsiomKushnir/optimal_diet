package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Recipe {
    private Long id;
    private String name;
    private com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe originalRecipe;
    private CalculationData calculation;
}
