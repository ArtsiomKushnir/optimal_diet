package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.IngredientDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Ingredient;
import com.kushnir.brstu.graduatework.optimaldiet.exception.ValidationException;
import com.kushnir.brstu.graduatework.optimaldiet.service.IngredientService;
import com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient.IngredientSearchCriteria;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

import static org.springframework.data.domain.Sort.Direction.ASC;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/ingredient")
public class IngredientController {

    private final IngredientService ingredientService;

    @GetMapping
    public Page<Ingredient> findAll(@RequestParam(required = false) final String name,
                                    @SortDefault(sort = "name", direction = ASC) final Pageable pageable) {
        final IngredientSearchCriteria ingredientSearchCriteria = IngredientSearchCriteria.builder()
                .name(name)
                .build();

        return ingredientService.findAll(pageable, ingredientSearchCriteria);
    }

    @PostMapping
    public Ingredient create(@RequestBody final IngredientDto ingredientDto) throws ValidationException {
        return ingredientService.create(ingredientDto);
    }

    @PutMapping
    public Ingredient update(@RequestBody final IngredientDto ingredientDto) throws ValidationException {
        return ingredientService.update(ingredientDto);
    }
}
