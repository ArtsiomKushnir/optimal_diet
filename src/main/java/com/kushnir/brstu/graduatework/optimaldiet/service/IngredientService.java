package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.IngredientDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Ingredient;
import com.kushnir.brstu.graduatework.optimaldiet.exception.ValidationException;
import com.kushnir.brstu.graduatework.optimaldiet.repository.IngredientRepository;
import com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient.IngredientSearchCriteria;
import com.kushnir.brstu.graduatework.optimaldiet.service.validator.IngredientValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import static com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient.IngredientSpecification.buildSpecification;

@Service
@RequiredArgsConstructor
public class IngredientService {

    private final IngredientRepository ingredientRepository;
    private final IngredientValidator ingredientValidator;

    public Page<Ingredient> findAll(final Pageable pageable, final IngredientSearchCriteria ingredientSearchCriteria) {
        final Specification<Ingredient> specification = buildSpecification(ingredientSearchCriteria);
        return ingredientRepository.findAll(specification, pageable);
    }

    public Ingredient create(final IngredientDto ingredientDto) throws ValidationException {
        ingredientValidator.validate(ingredientDto, false);

        final Ingredient ingredient = Ingredient.builder()
                .name(ingredientDto.getName())
                .calories(ingredientDto.getCalories())
                .protein(ingredientDto.getProtein())
                .fat(ingredientDto.getFat())
                .carbohydrates(ingredientDto.getCarbohydrates())
                .build();

        return ingredientRepository.save(ingredient);
    }

    public Ingredient update(final IngredientDto ingredientDto) throws ValidationException {
        ingredientValidator.validate(ingredientDto, true);

        final Ingredient ingredient = Ingredient.builder()
                .id(ingredientDto.getId())
                .name(ingredientDto.getName())
                .calories(ingredientDto.getCalories())
                .protein(ingredientDto.getProtein())
                .fat(ingredientDto.getFat())
                .carbohydrates(ingredientDto.getCarbohydrates())
                .build();

        return ingredientRepository.save(ingredient);
    }
}
