package com.kushnir.brstu.graduatework.optimaldiet.dto;

public enum Gender {
    MALE,
    FEMALE
}
