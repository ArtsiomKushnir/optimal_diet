package com.kushnir.brstu.graduatework.optimaldiet.exception;

import java.util.Map;

public interface BaseException {
    Map<String, String> getErrors();
}
