package com.kushnir.brstu.graduatework.optimaldiet.exception;

import lombok.*;

import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Builder
public class UserRegistrationException extends Exception implements BaseException {
    private Map<String, String> errors;
}
