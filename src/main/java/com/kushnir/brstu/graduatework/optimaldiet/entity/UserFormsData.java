package com.kushnir.brstu.graduatework.optimaldiet.entity;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Activity;
import com.kushnir.brstu.graduatework.optimaldiet.dto.DietAim;
import com.kushnir.brstu.graduatework.optimaldiet.dto.Gender;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class UserFormsData {

    @Column(name = "gender", table = "user_forms_data")
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Column(name = "age", table = "user_forms_data")
    private Integer age;

    @Column(name = "height", table = "user_forms_data")
    private Integer height;

    @Column(name = "weight", table = "user_forms_data")
    private Integer weight;

    @Column(name = "activity_level", table = "user_forms_data")
    @Enumerated(EnumType.STRING)
    private Activity activityLevel;

    @Column(name = "diet_aim", table = "user_forms_data")
    @Enumerated(EnumType.STRING)
    private DietAim dietAim;

    @Column(name = "selected_protein_coefficient", table = "user_forms_data")
    private Double selectedProteinCoefficient;

    @Column(name = "selected_fat_coefficient", table = "user_forms_data")
    private Double selectedFatCoefficient;
}
