package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.dto.MealTime;
import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.dto.SessionDataResponseDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.exception.MealTimeValidationException;
import com.kushnir.brstu.graduatework.optimaldiet.service.MealTimeService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserDietPlanService;
import com.kushnir.brstu.graduatework.optimaldiet.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.PLAN_KEY;
import static com.kushnir.brstu.graduatework.optimaldiet.rest.SessionController.USER_KEY;

@RestController
@RequestMapping("/api/mealTime")
@RequiredArgsConstructor
public class MealTimeController {

    private final MealTimeService mealTimeService;
    private final UserService userService;
    private final UserDietPlanService userDietPlanService;

    @PostMapping
    public SessionDataResponseDto addMealTime(@RequestBody final MealTime mealTime,
                                              @SessionAttribute(USER_KEY) User user,
                                              @SessionAttribute(PLAN_KEY) Plan sessionPlan) throws MealTimeValidationException {

        mealTimeService.addMealTime(mealTime, sessionPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }

    @DeleteMapping
    public SessionDataResponseDto deleteMealTime(@RequestParam final Short dayOfWeek,
                                                 @RequestParam final Integer weekDirectionId,
                                                 @RequestParam final Long orderId,
                                                 @SessionAttribute(USER_KEY) User user,
                                                 @SessionAttribute(PLAN_KEY) Plan sessionPlan) {

        mealTimeService.deleteMealTime(dayOfWeek, weekDirectionId, orderId, sessionPlan);

        if (user.getId() != null) {
            userService.saveUserData(user, sessionPlan);
            userDietPlanService.saveDietPlan(user, sessionPlan);
        }

        return SessionDataResponseDto.builder()
                .user(user)
                .plan(sessionPlan)
                .build();
    }
}
