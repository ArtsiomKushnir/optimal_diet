package com.kushnir.brstu.graduatework.optimaldiet.entity.converter;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Recipe;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static org.springframework.util.CollectionUtils.isEmpty;

public class RecipesIdListConverter implements AttributeConverter<ArrayList<Long>, String> {

    @Override
    public String convertToDatabaseColumn(final ArrayList<Long> recipeIds) {
        if (isEmpty(recipeIds)) return "";

        StringBuilder sb = new StringBuilder();

        String prefix = "";
        for (final Long recipeId : recipeIds) {
            sb.append(prefix);
            prefix = ",";
            sb.append(recipeId);
        }

        return sb.toString();
    }

    @Override
    public ArrayList<Long> convertToEntityAttribute(final String s) {
        if (isNotBlank(s)) {
            final String[] recipeIdsString = s.replaceAll(" ", "").split(",");

            final ArrayList<Long> recipeIds = new ArrayList<>();

            for (String recipeId : recipeIdsString) {
                recipeIds.add(Long.valueOf(recipeId));
            }

            return recipeIds;
        }

        return new ArrayList<>();
    }

    public static ArrayList<Long> convertIds(final Set<Recipe> recipes) {
        if (isEmpty(recipes)) return new ArrayList<>();
        return (ArrayList<Long>) recipes.stream().map(Recipe::getId).collect(Collectors.toList());
    }
}
