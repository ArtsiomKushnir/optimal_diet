package com.kushnir.brstu.graduatework.optimaldiet.repository;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RecipeRepository extends PagingAndSortingRepository<Recipe, Long>, JpaSpecificationExecutor<Recipe> {
}
