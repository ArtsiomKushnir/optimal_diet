package com.kushnir.brstu.graduatework.optimaldiet.entity.converter;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static org.springframework.util.CollectionUtils.isEmpty;

public class UnitGroupsConverter implements AttributeConverter<Set<String>, String> {

    @Override
    public String convertToDatabaseColumn(Set<String> strings) {
        if (isEmpty(strings)) return "";

        StringBuilder sb = new StringBuilder();

        String prefix = "";
        for (final String group : strings) {
            sb.append(prefix);
            prefix = ",";
            sb.append(group);
        }

        return sb.toString();
    }

    @Override
    public Set<String> convertToEntityAttribute(String s) {
        if (isNotBlank(s)) {
            final String[] groupStrings = s.replaceAll(" ", "").split(",");

            return new HashSet<>(Arrays.asList(groupStrings));
        }

        return null;
    }
}
