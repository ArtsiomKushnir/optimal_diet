package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.dto.UserDto;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Group;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.entity.UserFormsData;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserLoginException;
import com.kushnir.brstu.graduatework.optimaldiet.exception.UserRegistrationException;
import com.kushnir.brstu.graduatework.optimaldiet.repository.UserRepository;
import com.kushnir.brstu.graduatework.optimaldiet.service.mapper.UserDietPlanMapper;
import com.kushnir.brstu.graduatework.optimaldiet.service.mapper.UserFormsDataMapper;
import com.kushnir.brstu.graduatework.optimaldiet.service.util.Encoder;
import com.kushnir.brstu.graduatework.optimaldiet.service.validator.UserValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;


@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserValidator userValidator;
    private final UserDietPlanService userDietPlanService;
    private final UserFormsDataService userFormsDataService;

    public User register(final UserDto userDto, final Plan sessionPlan) throws UserRegistrationException {
        userValidator.validateRegistration(userDto);

        final User user = User.builder()
                .email(userDto.getEmail())
                .password(Encoder.encoding(userDto.getPassword()))
                .groups(Collections.singleton(Group.USER))
                .formsData(UserFormsDataMapper.map(sessionPlan))
                .enabled(true)
                .build();

        return userRepository.save(user);
    }

    public User login(final UserDto userDto, final Plan sessionPlan) throws UserLoginException {
        final User user = userValidator.validateLoginAndGet(userDto);
        userFormsDataService.mapUserFormsData(user, sessionPlan);
        userDietPlanService.mapUserDietPlan(user, sessionPlan);
        return user;
    }

    public void saveUserData(final User user, final Plan plan) {
        final UserFormsData userFormsData = UserFormsDataMapper.map(plan);
        user.setFormsData(userFormsData);
        userRepository.save(user);
    }
}
