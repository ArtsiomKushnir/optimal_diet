package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DayPlan {
    private Short dayOfWeek;
    private CalculationData calculation;
    private HashMap<Long, MealTime> mealTimes;
}
