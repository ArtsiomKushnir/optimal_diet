package com.kushnir.brstu.graduatework.optimaldiet.service.specification.recipe;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.List;

import static com.kushnir.brstu.graduatework.optimaldiet.service.specification.recipe.RecipeSpecificationBuilder.aPredicates;

public class RecipeSpecification {
    public static Specification<Recipe> buildSpecification(final RecipeSearchCriteria recipeSearchCriteria) {
        return (root, query, cb) -> {

            if(recipeSearchCriteria == null) {
                return null;
            }

            final List<Predicate> predicates = aPredicates(root, cb, query)
                    .withName(recipeSearchCriteria.getName())
                    .withDishId(recipeSearchCriteria.getDishId())
                    .withCategoryId(recipeSearchCriteria.getCategoryId())
                    .withUserId(recipeSearchCriteria.getUserId())
                    .build();

            return cb.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
