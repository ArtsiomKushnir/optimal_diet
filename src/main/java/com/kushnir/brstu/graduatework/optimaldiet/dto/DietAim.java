package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum DietAim {
    SUPPORT(1.0),
    INCREASE_5(1.05),
    INCREASE_10(1.1),
    INCREASE_15(1.15),
    LOSE_5(0.95),
    LOSE_10(0.9),
    LOSE_15(0.85);

    private final double index;
}
