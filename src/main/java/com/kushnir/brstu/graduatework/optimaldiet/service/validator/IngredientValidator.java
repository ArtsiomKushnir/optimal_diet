package com.kushnir.brstu.graduatework.optimaldiet.service.validator;

import com.kushnir.brstu.graduatework.optimaldiet.dto.IngredientDto;
import com.kushnir.brstu.graduatework.optimaldiet.exception.ValidationException;
import com.kushnir.brstu.graduatework.optimaldiet.repository.IngredientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.apache.logging.log4j.util.Strings.isBlank;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Component
@RequiredArgsConstructor
public class IngredientValidator {
    private final IngredientRepository ingredientRepository;

    public void validate(final IngredientDto ingredientDto, boolean isUpdate) throws ValidationException {
        final Map<String, String> errors = new HashMap<>();

        if (isBlank(ingredientDto.getName())) {
            errors.put("name", "Введите наименование ингредиента");
        } else if (ingredientDto.getName().length() > 250) {
            errors.put("name", "Превышено ограничение в 250 символов");
        } else if (!isUpdate && ingredientRepository.existsByName(ingredientDto.getName())) {
            errors.put("name", "Ингредиент с таким именем уже есть");
        }

        if (ingredientDto.getCalories() == null) {
            errors.put("calories", "Укажите значение");
        }
        if (ingredientDto.getProtein() == null) {
            errors.put("protein", "Укажите значение");
        }
        if (ingredientDto.getFat() == null) {
            errors.put("fat", "Укажите значение");
        }
        if (ingredientDto.getCarbohydrates() == null) {
            errors.put("carbohydrates", "Укажите значение");
        }

        if (ingredientDto.getUnit() == null) {
            errors.put("unit", "Выберите единицу измерения");
        }

        if (isNotEmpty(errors)) throw ValidationException.builder()
                .errors(errors)
                .build();
    }
}
