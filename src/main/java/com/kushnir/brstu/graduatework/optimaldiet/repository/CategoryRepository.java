package com.kushnir.brstu.graduatework.optimaldiet.repository;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Category;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface CategoryRepository extends PagingAndSortingRepository<Category, Integer> {
    List<Category> findAll();
    boolean existsByName(final String name);
}
