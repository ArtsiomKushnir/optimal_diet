package com.kushnir.brstu.graduatework.optimaldiet.service.validator;

import com.kushnir.brstu.graduatework.optimaldiet.dto.FormsData;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FormsDataValidator {

    private final PlanPersonalDataValidator planPersonalDataValidator;

    public void validate(final FormsData formsData) {
        planPersonalDataValidator.validate(formsData.getPersonData());
    }
}
