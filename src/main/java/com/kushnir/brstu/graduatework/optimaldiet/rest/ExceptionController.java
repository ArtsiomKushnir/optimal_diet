package com.kushnir.brstu.graduatework.optimaldiet.rest;

import com.kushnir.brstu.graduatework.optimaldiet.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@ControllerAdvice
@RestController
public class ExceptionController {
    @ExceptionHandler({UserRegistrationException.class, UserLoginException.class, MealTimeValidationException.class, ValidationException.class})
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public Map<String, String> handleUserExceptions(final BaseException e) {
        return e.getErrors();
    }
}
