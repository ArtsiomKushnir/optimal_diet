package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.CalculationData;
import com.kushnir.brstu.graduatework.optimaldiet.dto.Nutrients;
import com.kushnir.brstu.graduatework.optimaldiet.entity.Recipe;
import com.kushnir.brstu.graduatework.optimaldiet.entity.embedded.Ingredient;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.util.CollectionUtils.isEmpty;

@Service
public class RecipeCalculationService {
    public void calculate(final Recipe recipe) {

        List<Ingredient> ingredients = recipe.getIngredients();

        if (isEmpty(ingredients)) return;

        var kkal = 0L;
        var fats = 0.0;
        var proteins = 0.0;
        var carbohydrates = 0.0;

        for (final Ingredient ingredient : ingredients) {
            kkal += ingredient.getCalories() != null ? ingredient.getCalories().longValue() : 0L;
            proteins +=ingredient.getProtein() != null ? ingredient.getProtein().longValue() : 0L;
            fats += ingredient.getFat() != null ? ingredient.getFat().longValue() : 0L;
            carbohydrates += ingredient.getCarbohydrates() != null ? ingredient.getCarbohydrates().doubleValue() : 0L;
        }

        final CalculationData calculationData = CalculationData.builder()
                .calories(kkal)
                .nutrients(Nutrients.builder()
                        .protein(proteins)
                        .fats(fats)
                        .carbohydrates(carbohydrates)
                        .build())
                .build();

        recipe.setCalculation(calculationData);
    }
}
