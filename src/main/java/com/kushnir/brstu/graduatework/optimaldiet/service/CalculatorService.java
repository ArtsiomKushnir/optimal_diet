package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.*;
import com.kushnir.brstu.graduatework.optimaldiet.entity.embedded.Ingredient;
import com.kushnir.brstu.graduatework.optimaldiet.service.validator.PlanValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import static com.kushnir.brstu.graduatework.optimaldiet.dto.Gender.MALE;
import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

@Service
@RequiredArgsConstructor
public class CalculatorService {
    private final static int MALE_COEFFICIENT = 5;
    private final static int FEMALE_COEFFICIENT = -161;

    private final PlanValidator planValidator;

    public Plan calculate(final Plan plan) {
        final var weight = plan.getFormsData().getPersonData().getWeight();
        final var height = plan.getFormsData().getPersonData().getHeight();
        final var age = plan.getFormsData().getPersonData().getAge();
        final var gender = plan.getFormsData().getPersonData().getGender();
        final var activityLevel = plan.getFormsData().getPersonData().getActivityLevel();
        final var aim = plan.getFormsData().getDietAim();
        final var nutrientsAim = plan.getFormsData().getNutrientsAim();

        if (weight != null || height != null || age != null || gender != null) {

            long purposeGoal = 0L;

            var bmr = 10 * weight + 6.25 * height - 5 * age;

            var caloriesByGender = MALE.equals(gender)
                    ? Math.round(bmr + MALE_COEFFICIENT)
                    : Math.round(bmr - FEMALE_COEFFICIENT);

            if (activityLevel != null) {
                var caloriesByGenderAndActivity = Math.round(caloriesByGender * activityLevel.getIndex());
                plan.getCalculationResults().getBase().setCalories(caloriesByGenderAndActivity);

                if (aim != null) {
                    purposeGoal = Math.round(caloriesByGenderAndActivity * aim.getIndex());
                    plan.getCalculationResults().getCorrected().setCalories(purposeGoal);
                }

                if (nutrientsAim != null && purposeGoal > 0L) {
                    var proteinCoefficient = nutrientsAim.getProteinCoefficient();
                    var fatCoefficient = nutrientsAim.getFatCoefficient();

                    var pPermass = weight * proteinCoefficient; // грамм белка на массу тела
                    var fPermass = weight * fatCoefficient; // грамм жира на массу тела

                    var kkalsFromProtein = pPermass * 4; // кол-во калорий от белка
                    var kkalsFromFat = fPermass * 9; // кол-во калорий от жира

                    var remainKalories = purposeGoal - (kkalsFromProtein + kkalsFromFat);
                    var cPermass = remainKalories / 4; // грамм углеводов на массу тела
                    plan.getCalculationResults().getCorrected().getNutrients().setProtein(Math.round(pPermass * 100.0)/100.0);
                    plan.getCalculationResults().getCorrected().getNutrients().setFats(Math.round(fPermass * 100.0)/100.0);
                    plan.getCalculationResults().getCorrected().getNutrients().setCarbohydrates(Math.round(cPermass * 100.0)/100.0);
                }
            }
        }

        calculateMealTimes(plan);

        return plan;
    }

    public void calculate(final Plan plan, final Plan sessionPlan) {
        planValidator.validate(plan);

        final Plan calculatedPlan = calculate(plan);

        sessionPlan.setCalculationResults(calculatedPlan.getCalculationResults());
        sessionPlan.setFormsData(calculatedPlan.getFormsData());
        sessionPlan.setWeeks(calculatedPlan.getWeeks());
    }

    private void calculateMealTimes(final Plan plan) {
        final HashMap<Integer, WeekPlan> weeks = plan.getWeeks();
        if (weeks != null && weeks.size() > 0) {
            weeks.forEach((wk, wv) -> {

                plan.getCalculationResults().setOutOfAim(false);
                wv.setOutOfAim(false);

                final TreeMap<Short, DayPlan> days = wv.getDays();
                if (days != null && days.size() > 0) {
                    days.forEach((fd, vd) -> {
                        final HashMap<Long, MealTime> mealTimes = vd.getMealTimes();
                        if (mealTimes != null && mealTimes.size() > 0) {

                            if (vd.getCalculation() == null) {
                                vd.setCalculation(CalculationData.builder()
                                                .calories(0L)
                                                .nutrients(Nutrients.builder()
                                                        .fats(0.0)
                                                        .carbohydrates(0.0)
                                                        .protein(0.0)
                                                        .build())
                                        .build());
                            } else {
                                vd.getCalculation().setCalories(0L);
                                vd.getCalculation().getNutrients().setFats(0.0);
                                vd.getCalculation().getNutrients().setCarbohydrates(0.0);
                                vd.getCalculation().getNutrients().setProtein(0.0);
                            }

                            mealTimes.forEach((mtk, mtv) -> {

                                if (mtv.getCalculation() == null) {
                                    mtv.setCalculation(CalculationData.builder()
                                            .nutrients(Nutrients.builder().build())
                                            .build());
                                }

                                final Set<Recipe> recipes = mtv.getRecipes();
                                if (recipes != null && recipes.size() > 0) {
                                    CalculationData mealTimeCalculationData = mtv.getCalculation();

                                    Nutrients mealTimeNutrients = mealTimeCalculationData.getNutrients();

                                    var kkal = 0L;
                                    var fats = 0.0D;
                                    var proteins = 0.0D;
                                    var carbohydrates = 0.0D;

                                    for (Recipe recipe : recipes) {

                                        final List<Ingredient> ingredients = recipe.getOriginalRecipe().getIngredients();

                                        Double totalCalories = 0.0;
                                        double totalFat = 0.0;
                                        double totalProtein = 0.0;
                                        double totalCarbohydrates = 0.0;

                                        if (isNotEmpty(ingredients)) {
                                            for (Ingredient ingredient : ingredients) {
                                                Double unitCoefficient = ingredient.getUnitCoefficient();
                                                BigDecimal count = ingredient.getCount();

                                                BigDecimal recipeCalories = ingredient.getCalories();
                                                BigDecimal recipeFat = ingredient.getFat();
                                                BigDecimal recipeProtein = ingredient.getProtein();
                                                BigDecimal recipeCarbohydrates = ingredient.getCarbohydrates();

                                                totalCalories += ((recipeCalories.doubleValue() * unitCoefficient) * count.doubleValue());
                                                totalFat += ((recipeFat.doubleValue() * unitCoefficient) * count.doubleValue());
                                                totalProtein += ((recipeProtein.doubleValue() * unitCoefficient) * count.doubleValue());
                                                totalCarbohydrates += ((recipeCarbohydrates.doubleValue() * unitCoefficient) * count.doubleValue());
                                            }

                                            recipe.getCalculation().setCalories(totalCalories.longValue() / recipe.getOriginalRecipe().getNumberOfServings());
                                            recipe.getCalculation().getNutrients().setFats(totalFat / recipe.getOriginalRecipe().getNumberOfServings());
                                            recipe.getCalculation().getNutrients().setProtein(totalProtein / recipe.getOriginalRecipe().getNumberOfServings());
                                            recipe.getCalculation().getNutrients().setCarbohydrates(totalCarbohydrates / recipe.getOriginalRecipe().getNumberOfServings());
                                        }

                                        kkal += recipe.getCalculation().getCalories();
                                        fats += recipe.getCalculation().getNutrients().getFats();
                                        proteins += recipe.getCalculation().getNutrients().getProtein();
                                        carbohydrates += recipe.getCalculation().getNutrients().getCarbohydrates();
                                    }

                                    mealTimeCalculationData.setCalories(kkal);
                                    mealTimeNutrients.setFats(fats);
                                    mealTimeNutrients.setProtein(proteins);
                                    mealTimeNutrients.setCarbohydrates(carbohydrates);

                                    vd.getCalculation().setCalories(vd.getCalculation().getCalories() + mealTimeCalculationData.getCalories());
                                    vd.getCalculation().getNutrients().setFats(vd.getCalculation().getNutrients().getFats() + mealTimeNutrients.getFats());
                                    vd.getCalculation().getNutrients().setCarbohydrates(vd.getCalculation().getNutrients().getCarbohydrates() + mealTimeNutrients.getCarbohydrates());
                                    vd.getCalculation().getNutrients().setProtein(vd.getCalculation().getNutrients().getProtein() + mealTimeNutrients.getProtein());
                                }
                            });
                        }

                        if (plan.getCalculationResults().getCorrected().getCalories() != null && plan.getCalculationResults().getCorrected().getCalories() > 0) {
                            determineOutOfAim(plan, wv, vd);
                        }

                    });
                }
            });
        }
    }

    private void determineOutOfAim(final Plan plan, final WeekPlan weekPlan, final DayPlan dayPlan) {
        if (dayPlan.getCalculation().getCalories() > 0
                || dayPlan.getCalculation().getNutrients().getProtein() > 0
                || dayPlan.getCalculation().getNutrients().getFats() > 0
                || dayPlan.getCalculation().getNutrients().getCarbohydrates() > 0) {

            var kkal = plan.getCalculationResults().getCorrected().getCalories() - dayPlan.getCalculation().getCalories();
            var p = plan.getCalculationResults().getCorrected().getNutrients().getProtein() - dayPlan.getCalculation().getNutrients().getProtein();
            var f = plan.getCalculationResults().getCorrected().getNutrients().getFats() - dayPlan.getCalculation().getNutrients().getFats();
            var c = plan.getCalculationResults().getCorrected().getNutrients().getCarbohydrates() - dayPlan.getCalculation().getNutrients().getCarbohydrates();

            if (kkal > 150 || kkal < -150 || p < -30 || p > 30 || f < -20 || f > 20 || c < -40 || c > 40) {
                plan.getCalculationResults().setOutOfAim(true);
                weekPlan.setOutOfAim(true);
            }
        }
    }
}
