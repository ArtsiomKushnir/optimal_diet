package com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Ingredient;
import lombok.RequiredArgsConstructor;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@RequiredArgsConstructor
public class IngredientSpecificationBuilder {

    private final List<Predicate> predicates;
    private final Root<Ingredient> root;
    private final CriteriaBuilder cb;
    private final CriteriaQuery query;

    public static IngredientSpecificationBuilder aPredicates(final Root<Ingredient> root, final CriteriaBuilder cb, final CriteriaQuery query) {
        return new IngredientSpecificationBuilder(new ArrayList<>(), root, cb, query);
    }

    public IngredientSpecificationBuilder withName(final String name) {
        if (isNotBlank(name)) {
            predicates.add(cb.like(root.get("name"), "%" + name + "%"));
        }
        return this;
    }

    public List<Predicate> build() {
        return predicates;
    }
}
