package com.kushnir.brstu.graduatework.optimaldiet.service.specification.ingredient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class IngredientSearchCriteria {
    private String name;
}
