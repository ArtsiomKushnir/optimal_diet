package com.kushnir.brstu.graduatework.optimaldiet.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UnitBaseType {
    WEIGHT(1),
    VOLUME(2),
    NATURAL(3),
    LENGTH(4);

    private final Integer id;
}
