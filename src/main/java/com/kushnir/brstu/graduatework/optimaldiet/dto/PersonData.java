package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonData {
    private Gender gender;
    private Integer age;
    private Integer height;
    private Integer weight;
    private Activity activityLevel;
}
