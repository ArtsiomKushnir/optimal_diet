package com.kushnir.brstu.graduatework.optimaldiet.service;

import com.kushnir.brstu.graduatework.optimaldiet.dto.Plan;
import com.kushnir.brstu.graduatework.optimaldiet.entity.User;
import com.kushnir.brstu.graduatework.optimaldiet.service.mapper.UserFormsDataMapper;
import org.springframework.stereotype.Service;

@Service
public class UserFormsDataService {
    public void mapUserFormsData(final User from, final Plan to) {
        UserFormsDataMapper.map(from, to);
    }
}
