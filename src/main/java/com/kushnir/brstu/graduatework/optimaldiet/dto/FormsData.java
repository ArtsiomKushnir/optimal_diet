package com.kushnir.brstu.graduatework.optimaldiet.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FormsData {
    private PersonData personData;
    private DietAim dietAim;
    private NutrientsCoefficient nutrientsAim;
}
