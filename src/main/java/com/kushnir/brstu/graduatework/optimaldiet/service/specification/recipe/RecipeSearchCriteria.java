package com.kushnir.brstu.graduatework.optimaldiet.service.specification.recipe;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RecipeSearchCriteria {
    private Integer categoryId;
    private Integer dishId;
    private String name;
    private Long userId;
}
