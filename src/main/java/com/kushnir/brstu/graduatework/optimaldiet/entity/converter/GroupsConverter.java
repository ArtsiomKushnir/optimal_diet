package com.kushnir.brstu.graduatework.optimaldiet.entity.converter;

import com.kushnir.brstu.graduatework.optimaldiet.entity.Group;

import javax.persistence.AttributeConverter;
import java.util.HashSet;
import java.util.Set;

import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static org.springframework.util.CollectionUtils.isEmpty;

public class GroupsConverter implements AttributeConverter<Set<Group>, String> {
    @Override
    public String convertToDatabaseColumn(final Set<Group> attribute) {
        if (isEmpty(attribute)) return "";

        StringBuilder sb = new StringBuilder();

        String prefix = "";
        for (final Group group : attribute) {
            sb.append(prefix);
            prefix = ",";
            sb.append(group.name());
        }

        return sb.toString();
    }

    @Override
    public Set<Group> convertToEntityAttribute(final String dbData) {

        if (isNotBlank(dbData)) {
            final String[] groupStrings = dbData.replaceAll(" ", "").split(",");

            final Set<Group> groups = new HashSet<>();

            for (String groupString : groupStrings) {
                groups.add(Group.valueOf(groupString));
            }

            return groups;
        }

        return null;
    }
}
