import RecipeDetailsModal from './modal/RecipeDetailsModal.vue.js'

export default {
    name: 'RecipesView',
    components: {
        RecipeDetailsModal
    },
    data() {
        return {
            recipes: [],

            page: {
                content: [],
                sort: null,
                pageable: {
                    sort: null,
                    offset: 0,
                    pageNumber: 0,
                    pageSize: 10,
                    paged: null,
                    unpaged: null
                },
                last: null,
                first: null,
                totalPages: null,
                totalElements: null,
                number: null,
                size: null,
                numberOfElements: null,
                empty: null
            }
        }
    },
    mounted() {
        this.searchRecipes();
    },
    methods: {
        getRecipes(params) {
            axios
                .get('/api/recipes',
                    {
                        params: params,
                        timeout: 3000
                    }
                )
                .then(response => {
                    this.page = response.data
                    this.recipes = this.page.content;
                })
                .catch(error => console.log(error));
        },
        searchRecipes() {
            if (this.searchName || this.selectedCategory || this.selectedDish) {
                this.getRecipes({
                    name:       this.searchName,
                    dishId:     this.selectedDish ? this.selectedDish.id : null,
                    categoryId: this.selectedCategory ? this.selectedCategory.id : null,
                    page:       this.page.pageable.pageNumber,
                    size:       this.page.pageable.pageSize
                });
            } else {
                this.getRecipes({
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                });
            }
        },
        openRecipeDetailsModal(id) {
            this.$refs.RecipeDetailsModal.init(id);
            new bootstrap.Modal(document.getElementById('recipeDetailsModal'), {}).show();
        },
        previousPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        nextPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber + 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        toPage(n) {
            this.page.pageable.pageNumber = n - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
    },
    template:
        '                       <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">' +
        '                           <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">' +
        '                               <h1 class="h2">Рецепты</h1>' +
        '                               <div class="btn-toolbar mb-2 mb-md-0">' +
        '                                   <div class="btn-group me-2">' +
        '                                   </div>' +
        '                               </div>' +
        '                           </div>' +
        '                           <div class="table-responsive">' +
        '                               <table class="table table-striped table-hover table-sm">' +
        '                                   <thead>' +
        '                                       <tr>' +
        '                                           <th>#</th>' +
        '                                           <th>Название</th>' +
        '                                           <th>Кол-во ингредиентов</th>' +
        '                                           <th>ккал</th>' +
        '                                           <th>Белков</th>' +
        '                                           <th>Жиров</th>' +
        '                                           <th>Углеводов</th>' +
        '                                       </tr>' +
        '                                   </thead>' +
        '                                   <tbody>' +
        '                                       <tr v-for="recipe in recipes" @click="openRecipeDetailsModal(recipe.id)">' +
        '                                           <td>{{ recipe.id }}</td>' +
        '                                           <td>{{ recipe.name }}</td>' +
        '                                           <td class="text-center">{{ recipe.recipeIngredients.length }}</td>' +
        '                                           <td class="text-center">{{ recipe.calculation ? recipe.calculation.calories : \'-\' }}</td>' +
        '                                           <td class="text-center">{{ recipe.calculation ? recipe.calculation.calories.nutrients ? recipe.calculation.calories.nutrients.protein : \'-\' : \'-\' }}</td>' +
        '                                           <td class="text-center">{{ recipe.calculation ? recipe.calculation.calories.nutrients ? recipe.calculation.calories.nutrients.fats : \'-\' : \'-\' }}</td>' +
        '                                           <td class="text-center">{{ recipe.calculation ? recipe.calculation.calories.nutrients ? recipe.calculation.calories.nutrients.carbohydrates : \'-\' : \'-\' }}</td>' +
        '                                       </tr>' +
        '                                   </tbody>' +
        '                               </table>' +
        '                           </div>' +

        '                           <nav v-if="!page.empty && (!page.last || !page.first)">' +
        '                               <ul class="pagination justify-content-center pagination-sm">' +

        '                                   <template v-if="page.first">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link" tabindex="-1" aria-disabled="true">&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="previousPage" >&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                                   <template v-for="n in page.totalPages">' +
        '                                       <template v-if="n === page.number + 1">' +
        '                                           <li class="page-item active" aria-current="page">' +
        '                                               <span class="page-link">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                       <template v-else>' +
        '                                           <li class="page-item">' +
        '                                               <span class="page-link" @click="toPage(n)">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                   </template>' +

        '                                   <template v-if="page.last">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="nextPage">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                               </ul>' +
        '                           </nav>' +
        '' +
        '                           <recipe-details-modal ref="RecipeDetailsModal"></recipe-details-modal>' +

        '                       </main>'
}