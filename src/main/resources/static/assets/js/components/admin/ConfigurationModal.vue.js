import RecipesView from './RecipesView.vue.js';
import IngredientsView from './IngredientsView.vue.js';
import UsersView from './UsersView.vue.js'

export default {
    name: 'ConfigurationModal',
    components: {
        RecipesView,
        IngredientsView,
        UsersView
    },
    props: {
        user: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            showModal: false,
            selectedView: 1
        }
    },
    methods: {
        openModal() {
            this.showModal = true;
        },
        closeModal() {
            this.showModal = false;
            this.selectedView = 1
        },
        goToView(id) {
            this.selectedView = id;
        }
    },
    template:
        '<div v-if="showModal" class="configuration-modal" ' +
        '       id="сonfigurationModal" ' +
        '       tabindex="-1" >' +
        '   <div class="configuration-modal-dialog">' +
        '       <div class="configuration-modal-content">' +
        '           <div class="configuration-modal-header">' +
        '               <h5 class="configuration-modal-title h4" >Панель администратора</h5>' +
        '               <button type="button" class="btn-close" aria-label="Закрыть окно настроек" @click="closeModal"></button>' +
        '           </div>' +
        '           <div class="configuration-modal-body">' +
        '               <div class="container-fluid">' +
        '                   <div class="row">' +
        '                       <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">' +
        '                           <div class="position-sticky pt-3">' +
        '                               <ul class="nav flex-column">' +
        '                                   <li class="nav-item">' +
        '                                       <a class="nav-link" ' +
        '                                           :class="selectedView === 1 ? \'active\' : \'\'"' +
        '                                           href="#"' +
        '                                           @click="goToView(1)">Рецепты</a>' +
        '                                   </li>' +
        '                                   <li class="nav-item">' +
        '                                       <a class="nav-link" ' +
        '                                           :class="selectedView === 2 ? \'active\' : \'\'"' +
        '                                           href="#"' +
        '                                           @click="goToView(2)">Ингредиенты</a>' +
        '                                   </li>' +
        '                                   <li class="nav-item">' +
        '                                       <a class="nav-link" ' +
        '                                           :class="selectedView === 3 ? \'active\' : \'\'"' +
        '                                           href="#"' +
        '                                           @click="goToView(3)">Пользователи</a>' +
        '                                   </li>' +
        '                               </ul>' +
        '                           </div>' +
        '                       </nav>' +

        '                       <template v-if="selectedView === 1">' +
        '                           <recipes-view></recipes-view>' +
        '                       </template>' +
        '                       <template v-else-if="selectedView === 2">' +
        '                           <ingredients-view></ingredients-view>' +
        '                       </template>' +
        '                       <template v-else-if="selectedView === 3">' +
        '                           <users-view></users-view>' +
        '                       </template>' +

        '                   </div>' +
        '               </div>' +
        '           </div>' +
        '           <div class="configuration-modal-footer">' +
        '               <button type="button" class="btn btn-secondary" @click="closeModal">Закрыть панель администратора</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>'
}