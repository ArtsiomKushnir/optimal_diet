import AddNewIngredientModal from './modal/AddNewIngredientModal.vue.js'

export default {
    name: 'IngredientsView',
    components: {
        AddNewIngredientModal
    },
    data() {
        return {
            ingredients: [],
            searchName: null,
            page: {
                content: [],
                sort: null,
                pageable: {
                    sort: null,
                    offset: 0,
                    pageNumber: 0,
                    pageSize: 10,
                    paged: null,
                    unpaged: null
                },
                last: null,
                first: null,
                totalPages: null,
                totalElements: null,
                number: null,
                size: null,
                numberOfElements: null,
                empty: null
            }
        }
    },
    mounted() {
        this.searchIngredients();
    },
    methods: {
        searchIngredients() {
            if (this.searchName) {
                this.getIngredientsPageable({
                    name: this.searchName,
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                });
            } else {
                this.getIngredientsPageable({
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                });
            }
        },
        getIngredientsPageable(params) {
            axios
                .get('/api/ingredient',
                    {
                        params: params,
                        timeout: 3000
                    }
                )
                .then(response => {
                    this.page = response.data
                    this.ingredients = this.page.content;
                })
                .catch(error => console.log(error));
        },
        previousPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        nextPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber + 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        toPage(n) {
            this.page.pageable.pageNumber = n - 1;
            this.getRecipes({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        }
    },
    template:
        '                       <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">' +
        '                           <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">' +
        '                               <h1 class="h2">Ингредиенты</h1>' +
        '                               <div class="btn-toolbar mb-2 mb-md-0">' +
        '                                   <div class="btn-group me-2">' +
        '                                       <button class="btn btn-success" ' +
        '                                               data-bs-target="#addNewIngredientModal" ' +
        '                                               data-bs-toggle="modal" ' +
        '                                               data-bs-dismiss="modal">+ Добавить ингредиент</button>' +
        '                                   </div>' +
        '                               </div>' +
        '                           </div>' +
        '                           <div class="table-responsive">' +
        '                               <table class="table table-striped table-sm">' +
        '                                   <thead>' +
        '                                       <tr>' +
        '                                           <th>#</th>' +
        '                                           <th>Название</th>' +
        '                                           <th>Калории</th>' +
        '                                           <th>Жиры</th>' +
        '                                           <th>Белки</th>' +
        '                                           <th>Углеводы</th>' +
        '                                           <th>ед. измерения</th>' +
        '                                       </tr>' +
        '                                   </thead>' +
        '                                   <tbody>' +
        '                                       <tr v-for="ingredient in ingredients">' +
        '                                           <td>{{ ingredient.id }}</td>' +
        '                                           <td>{{ ingredient.name }}</td>' +
        '                                           <td>{{ ingredient.calories }}</td>' +
        '                                           <td>{{ ingredient.fat }}</td>' +
        '                                           <td>{{ ingredient.protein }}</td>' +
        '                                           <td>{{ ingredient.carbohydrates }}</td>' +
        '                                           <td>{{ ingredient.unit.name }}</td>' +
        '                                       </tr>' +
        '                                   </tbody>' +
        '                               </table>' +
        '                           </div>' +
        '                           <nav v-if="!page.empty && (!page.last || !page.first)">' +
        '                               <ul class="pagination justify-content-center pagination-sm">' +

        '                                   <template v-if="page.first">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link" tabindex="-1" aria-disabled="true">&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="previousPage" >&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                                   <template v-for="n in page.totalPages">' +
        '                                       <template v-if="n === page.number + 1">' +
        '                                           <li class="page-item active" aria-current="page">' +
        '                                               <span class="page-link">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                       <template v-else>' +
        '                                           <li class="page-item">' +
        '                                               <span class="page-link" @click="toPage(n)">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                   </template>' +

        '                                   <template v-if="page.last">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="nextPage">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                               </ul>' +
        '                           </nav>' +
        '                       <add-new-ingredient-modal></add-new-ingredient-modal>' +
        '                       </main>'
}