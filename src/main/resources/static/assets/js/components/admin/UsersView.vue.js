export default {
    name: 'UsersView',
    components: {},
    data() {
        return {}
    },
    methods: {},
    template:
        '                       <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">' +
        '                           <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">' +
        '                               <h1 class="h2">Пользователи</h1>' +
        '                               <div class="btn-toolbar mb-2 mb-md-0">' +
        '                                   <div class="btn-group me-2">' +
        '                                   </div>' +
        '                               </div>' +
        '                           </div>' +
        '                           <div class="table-responsive">' +
        '                               <table class="table table-striped table-sm">' +
        '                                   <thead>' +
        '                                       <tr>' +
        '                                           <th>#</th>' +
        '                                           <th>Header</th>' +
        '                                           <th>Header</th>' +
        '                                           <th>Header</th>' +
        '                                           <th>Header</th>' +
        '                                       </tr>' +
        '                                   </thead>' +
        '                                   <tbody>' +
        '                                       <tr>' +
        '                                           <td>1,001</td>' +
        '                                           <td>random</td>' +
        '                                           <td>data</td>' +
        '                                           <td>placeholder</td>' +
        '                                           <td>text</td>' +
        '                                       </tr>' +
        '                                   </tbody>' +
        '                               </table>' +
        '                           </div>' +
        '                       </main>'
}