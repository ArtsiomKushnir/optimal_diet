import fieldValidationEntity from '../../entity/fieldValidationEntity.js'

export default {
    name: 'AddNewIngredientModal',
    data() {
        return {
            units: [],
            ingredient: {
                name: null,
                calories: null,
                fat: null,
                protein: null,
                carbohydrates: null,
                unit: {

                }
            },
            validation: {
                name: fieldValidationEntity(),
                calories: fieldValidationEntity(),
                fat: fieldValidationEntity(),
                protein: fieldValidationEntity(),
                carbohydrates: fieldValidationEntity(),
                unit: fieldValidationEntity(),
            },

            unitStr: function () {
                return this.ingredient.unit.id ? this.ingredient.unit.id !== 3 ? '100 ' + this.ingredient.unit.shortName : '1 ' + this.ingredient.unit.shortName : 'ед.изм';
            }
        }
    },
    mounted() {
        this.getUnits();
    },
    methods: {
        getUnits() {
            axios
                .get('/api/unit')
                .then(response => this.units = response.data)
                .catch(error => console.log(error));
        },
        onModalClose() {
            this.ingredient = {
                name: null,
                calories: null,
                protein: null,
                fat: null,
                carbohydrates: null,
                unit: {}
            };
            this.validation = {
                name: fieldValidationEntity(),
                calories: fieldValidationEntity(),
                protein: fieldValidationEntity(),
                fat: fieldValidationEntity(),
                carbohydrates: fieldValidationEntity(),
                unit: fieldValidationEntity(),
            };
        },
        addNew() {
            axios
                .post(
                    '/api/ingredient',
                    this.ingredient
                )
                .then(response => {
                    this.onModalClose();
                    bootstrap.Modal.getInstance(document.getElementById('addNewIngredientModal')).hide();
                    this.$parent.searchIngredients();
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.name) {
                            this.validation.name.errorMsg = error.response.data.name;
                            this.validation.name.validate();
                        }
                        if (error.response.data.calories) {
                            this.validation.calories.errorMsg = error.response.data.calories;
                            this.validation.calories.validate();
                        }
                        if (error.response.data.protein) {
                            this.validation.protein.errorMsg = error.response.data.protein;
                            this.validation.protein.validate();
                        }
                        if (error.response.data.fat) {
                            this.validation.fat.errorMsg = error.response.data.fat;
                            this.validation.fat.validate();
                        }
                        if (error.response.data.carbohydrates) {
                            this.validation.carbohydrates.errorMsg = error.response.data.carbohydrates;
                            this.validation.carbohydrates.validate();
                        }
                        if (error.response.data.unit) {
                            this.validation.unit.errorMsg = error.response.data.unit;
                            this.validation.unit.validate();
                        }
                    }
                });
        }
    },
    template:
    '<div>' +
        '<div class="modal fade" ' +
        'id="addNewIngredientModal" ' +
        'data-bs-backdrop="static" ' +
        'data-bs-keyboard="false" ' +
        'aria-labelledby="addNewIngredientModallLabel" aria-hidden="true">' +
        '  <div class="modal-dialog">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <h5 class="modal-title" id="addNewIngredientModallLabel">Добавление нового ингредиента</h5>' +
        '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click="onModalClose"></button>' +
        '      </div>' +
        '      <div class="modal-body">' +

        '               <form class="needs-validation" novalidate>' +

        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.name.valid ? \'is-valid\' : validation.name.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientName" ' +
        '                               placeholder="Наименование" ' +
        '                               @change="validation.name.clean()" ' +
        '                               v-model="ingredient.name"' +
        '                               required aria-describedby="addNewIngredientNameValidationFeedback">' +
        '                       <label for="addNewIngredientName">Наименование</label>' +
        '                       <div v-if="validation.name.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientNameValidationFeedback">' +
        '                               {{ validation.name.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.calories.valid ? \'is-valid\' : validation.calories.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientCalories" ' +
        '                               placeholder="Калори (г./ед.изм )" ' +
        '                               @change="validation.calories.clean()" ' +
        '                               v-model="ingredient.calories"' +
        '                               required aria-describedby="addNewIngredientCaloriesValidationFeedback">' +
        '                       <label for="addNewIngredientCalories">Калори (ккал/ {{ unitStr() }})</label>' +
        '                       <div v-if="validation.calories.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientCaloriesValidationFeedback">' +
        '                               {{ validation.calories.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.protein.valid ? \'is-valid\' : validation.protein.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientProtein" ' +
        '                               placeholder="Белков (г./ед.изм )" ' +
        '                               @change="validation.protein.clean()" ' +
        '                               v-model="ingredient.protein"' +
        '                               required aria-describedby="addNewIngredientProteinValidationFeedback">' +
        '                       <label for="addNewIngredientProtein">Белков (г/ {{ unitStr() }})</label>' +
        '                       <div v-if="validation.protein.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientProteinValidationFeedback">' +
        '                               {{ validation.protein.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.fat.valid ? \'is-valid\' : validation.fat.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientFat" ' +
        '                               placeholder="Жиров (г./ед.изм )" ' +
        '                               @change="validation.fat.clean()" ' +
        '                               v-model="ingredient.fat"' +
        '                               required aria-describedby="addNewIngredientFatValidationFeedback">' +
        '                       <label for="addNewIngredientFat">Жиров (г/ {{ unitStr() }})</label>' +
        '                       <div v-if="validation.fat.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientFatValidationFeedback">' +
        '                               {{ validation.fat.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.carbohydrates.valid ? \'is-valid\' : validation.carbohydrates.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientСarbohydrates" ' +
        '                               placeholder="Углеводов (г./ед.изм )" ' +
        '                               @change="validation.carbohydrates.clean()" ' +
        '                               v-model="ingredient.carbohydrates"' +
        '                               required aria-describedby="addNewIngredientСarbohydratesValidationFeedback">' +
        '                       <label for="addNewIngredientСarbohydrates">Углеводов (г/ {{ unitStr() }})</label>' +
        '                       <div v-if="validation.carbohydrates.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientСarbohydratesValidationFeedback">' +
        '                               {{ validation.carbohydrates.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '                   <div class="form-floating">' +
        '                       <select class="form-select form-select-sm"' +
        '                               :class="[ validation.unit.valid ? \'is-valid\' : validation.unit.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="addNewIngredientUnit"' +
        '                               v-model="ingredient.unit" ' +
        '                               @change="validation.unit.clean()"' +
        '                               required aria-describedby="addNewIngredientUnit addNewIngredientUnitValidationFeedback">' +
        '                           <option v-for="unit in units" :value="unit" >' +
        '                               {{ unit.name }} ({{ unit.shortName }})' +
        '                           </option>' +
        '                       </select>' +
        '                       <label for="addNewIngredientUnit">Выберите единицу измерения</label>' +
        '                       <div v-show="validation.unit.valid === false" class="invalid-feedback" ' +
        '                               id="addNewIngredientUnitValidationFeedback">' +
        '                           {{ validation.unit.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +

        '               </form>' +

        '      </div>' +
        '      <div class="modal-footer">' +
        '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Отмена</button>' +
        '        <button type="button" class="btn btn-success" @click="addNew">Добавить</button>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>' +
        '</div>'
}
