import mealTimeValidationEntity from './entity/mealTimeValidationEntity.js'

export default {
    name: 'AddMealTimeModal',
    props: {
        user: {
            required: true,
            type: Object
        },
        plan: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            dayName: null,

            mealTime: {
                id: null,
                weekDirectionId: null,
                dayOfWeek: null,
                orderId: null,
                time: null,
                name: null,
            },

            mealTimeValidate: mealTimeValidationEntity(),
        }
    },
    methods: {
        init(dayOfWeek, dayName, weekDirectionId) {
            this.mealTime.dayOfWeek = dayOfWeek;
            this.dayName = dayName
            this.mealTime.weekDirectionId = weekDirectionId;
        },
        confirmAdd() {
            axios
                .post(
                    '/api/mealTime',
                    this.mealTime
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.user.id = response.data.user.id;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                    this.onModalClose();
                    bootstrap.Modal.getInstance(document.getElementById('addMealTimeModal')).hide();
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.name) {
                            this.mealTimeValidate.name.errorMsg = error.response.data.name;
                            this.mealTimeValidate.name.validate();
                        }
                        if (error.response.data.time) {
                            this.mealTimeValidate.time.errorMsg = error.response.data.time;
                            this.mealTimeValidate.time.validate();
                        }
                    }
                });
        },
        onModalClose() {
            this.dayName = null

            this.mealTime = {
                id: null,
                weekDirectionId: null,
                dayOfWeek: null,
                orderId: null,
                time: null,
                name: null,
            };
        }
    },
    template:
        '<div class="modal fade" id="addMealTimeModal" ' +
        'data-bs-backdrop="static" ' +
        'data-bs-keyboard="false" ' +
        'tabindex="-1" ' +
        'aria-labelledby="addMealTimeModalLabel" aria-hidden="true">' +
        '  <div class="modal-dialog">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <h5 class="modal-title" id="addMealTimeModalLabel">Добавление времени приема пищи для: Неделя - {{ mealTime.weekDirectionId }}, день - {{ dayName }}</h5>' +
        '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click="onModalClose"></button>' +
        '      </div>' +
        '      <div class="modal-body">' +

        '               <form class="needs-validation" novalidate>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ mealTimeValidate.name.valid ? \'is-valid\' : mealTimeValidate.name.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="mealTimeName" ' +
        '                               placeholder="Обозначение" ' +
        '                               @change="mealTimeValidate.name.clean()" ' +
        '                               v-model="mealTime.name"' +
        '                               required aria-describedby="mealTimeNamenValidationFeedback">' +
        '                       <label for="mealTimeName">Обозначение</label>' +
        '                       <div v-if="mealTimeValidate.name.valid === false" class="invalid-feedback" ' +
        '                               id="mealTimeNamenValidationFeedback">' +
        '                               {{ mealTimeValidate.name.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="time" ' +
        '                               class="form-control" ' +
        '                               :class="[ mealTimeValidate.time.valid ? \'is-valid\' : mealTimeValidate.time.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="mealTimeTime" ' +
        '                               placeholder="Время приема пищи" ' +
        '                               @change="mealTimeValidate.time.clean()" ' +
        '                               v-model="mealTime.time"' +
        '                               required aria-describedby="mealTimeTimeValidationFeedback">' +
        '                       <label for="mealTimeTime">Время приема пищи</label>' +
        '                       <div v-if="mealTimeValidate.time.valid === false" class="invalid-feedback" ' +
        '                               id="mealTimeTimeValidationFeedback">' +
        '                               {{ mealTimeValidate.time.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '               </form>' +

        '      </div>' +
        '      <div class="modal-footer">' +
        '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Отмена</button>' +
        '        <button type="button" class="btn btn-success" @click="confirmAdd">Добавить время приема пищи</button>' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>'
}
