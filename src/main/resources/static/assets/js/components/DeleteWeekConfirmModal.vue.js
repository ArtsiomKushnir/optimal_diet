export default {
    name: 'DeleteWeekConfirmModal',
    props: {
        user: {
            required: true,
            type: Object
        },
        plan: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            directionId: null
        }
    },
    methods: {
        init(directionId) {
            this.directionId = directionId;
        },
        onModalClose() {
            this.directionId = null;
        },
        confirmDelete() {
            axios
                .delete(
                    '/api/plan/week?directionId=' + this.directionId
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.user.id = response.data.user.id;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                    this.onModalClose();
                    bootstrap.Modal.getInstance(document.getElementById('deleteWeekConfirmModal')).hide();
                })
                .catch(error => console.log(error.response))
        }
    },
    template:
    '<div class="modal fade" ' +
        'id="deleteWeekConfirmModal" ' +
        'data-bs-backdrop="static" ' +
        'data-bs-keyboard="false" ' +
        'tabindex="-1" ' +
        'aria-labelledby="deleteWeekConfirmModalLabel" aria-hidden="true">' +
        '  <div class="modal-dialog">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <h5 class="modal-title" id="deleteWeekConfirmModalLabel">Подтверждение удаления недели</h5>' +
        '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click="onModalClose"></button>' +
        '      </div>' +
        '      <div class="modal-body">' +
        '        Неделя {{ directionId }} содержит данные о рационе питания.\n' +
        '        Вы уверены что ходите удалить неделю {{ directionId }}?' +
        '      </div>' +
        '      <div class="modal-footer">' +
        '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Отмена</button>' +
        '        <button type="button" class="btn btn-warning" @click="confirmDelete">Удалить неделю {{ directionId }}</button>' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>'
}