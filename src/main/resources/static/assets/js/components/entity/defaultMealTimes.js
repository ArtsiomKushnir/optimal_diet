import defaultCalculationData from './defaultCalculationData.js'

const defaultMealTimes = function() {
    return [
        {
            id: null,
            weekDirectionId: null,
            dayOfWeek: null,
            order: null,
            time: null,
            name: null,
            rationVolume: null,
            calculation: defaultCalculationData(),
            recipes: []
        },
        {
            id: null,
            order: null,
            time: null,
            name: null,
            rationVolume: null,
            calculation: defaultCalculationData(),
            recipes: []
        },
        {
            id: null,
            order: null,
            time: null,
            name: null,
            rationVolume: null,
            calculation: defaultCalculationData(),
            recipes: []
        }
    ]
};

export default defaultMealTimes;