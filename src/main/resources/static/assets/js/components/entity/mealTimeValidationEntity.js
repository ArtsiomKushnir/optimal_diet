import fieldValidationEntity from './fieldValidationEntity.js'

const mealTimeValidationEntity = function() {
    return {
        name: fieldValidationEntity(),
        time: fieldValidationEntity(),

        cleanAll() {
            this.name.clean();
            this.time.clean();
        }
    };
};

export default mealTimeValidationEntity;