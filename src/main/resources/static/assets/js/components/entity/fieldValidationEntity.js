const fieldValidationEntity = function() {
    return {
        errorMsg: null,
        valid: null,
        clean() {
            this.valid = null;
            this.errorMsg = null;
        },
        validate() {
            if (this.errorMsg) this.valid = false;
        }
    };
};

export default fieldValidationEntity;