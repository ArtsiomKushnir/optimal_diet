const defaultCalculationData = function() {
    return {
        calories: null,
        nutrients: {
            protein: null,
            fats: null,
            carbohydrates: null
        }
    }
};

export default defaultCalculationData;