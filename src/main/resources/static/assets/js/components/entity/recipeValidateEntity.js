import fieldValidationEntity from './fieldValidationEntity.js'

const recipeValidateEntity = function() {
    return {
        name: fieldValidationEntity(),
        dish: fieldValidationEntity(),
        cookingTimeMin: fieldValidationEntity(),
        numberOfServings: fieldValidationEntity(),
        ingredients: fieldValidationEntity(),
        steps: fieldValidationEntity(),

        cleanAll() {
            this.name.clean();
            this.dish.clean();
            this.cookingTimeMin.clean();
            this.numberOfServings.clean();
            this.ingredients.clean();
        }
    };
};

export default recipeValidateEntity;