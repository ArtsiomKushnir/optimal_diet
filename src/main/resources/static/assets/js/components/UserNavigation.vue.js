export default {
    name: 'UserNavigation',
    props: {
        user: {
            required: true,
            type: Object
        },
        plan: {
            required: true,
            type: Object
        }
    },
    data() {
        return {

        }
    },
    mounted() {
        this.getUser();
    },
    methods: {
        getUser() {
            axios
                .get(
                    '/api/user'
                )
                .then(response => {
                    this.user.email = response.data.email;
                    this.user.id = response.data.id;
                    this.user.groups = response.data.groups;
                })
                .catch(error => console.log(error.response))
        },
        logout() {
            axios
                .post(
                    '/api/user/logout'
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.user.id = response.data.user.id;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                })
                .catch(error => {
                    console.log(error);
                });
        },
        openConfigurationModal() {
            this.$parent.$refs.ConfigurationModal.openModal();
        }
    },
    template:
        '<template v-if="user.email">' +
        '   <div class="flex-shrink-0 dropdown">' +
        '       <a href="#" class="d-block link-dark text-decoration-none dropdown-toggle"' +
        '           id="dropdownUser"' +
        '           data-bs-toggle="dropdown"' +
        '           aria-expanded="false">' +
        '           <i class="bx bx-user-circle bx-md pt-3"></i>' +
        '       </a>' +
        '       <ul class="dropdown-menu text-small shadow" aria-labelledby="dropdownUser">' +
        '           <template v-if="user.groups.includes(\'ADMIN\')">' +
        '               <li>' +
        '                   <a class="dropdown-item" href="#" @click="openConfigurationModal">Панель управления</a>' +
        '               </li>' +
        '               <li><hr class="dropdown-divider"></li>' +
        '           </template>' +
        '           <li><a class="dropdown-item" href="#" @click="logout">Выйти</a></li>' +
        '       </ul>' +
        '   </div>' +
        '</template>' +
        '<template v-else>' +
        '   <button type="button"' +
        '           class="btn btn-outline-primary ms-3"' +
        '           data-bs-toggle="modal"' +
        '           data-bs-target="#userLoginModal">Войти</button>' +
        '</template>'
}