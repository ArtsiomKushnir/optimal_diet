export default {
    name: 'EditStepList',
    props: {
        recipe: {
            required: true,
            type: Object
        }
    },
    data() {
        return {

        }
    },
    methods: {
        deleteStep(index) {
            this.recipe.steps.splice(index,1);
        },
        addStep() {
            this.recipe.steps.push({
                id: this.recipe.steps.length + 1,
                description: null,
            })
        }
    },
    template:
        '<div>' +
        '            <div class="row g-2 mb-3" v-for="(step, index) in recipe.steps">' +
        '              <div class="col-1">' +
        '                   <span>{{ step.id }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <textarea ' +
        '                           class="form-control" ' +
        '                           placeholder="Шаг инструкции" ' +
        '                           style="height: 50px"' +
        '                           v-model="step.description">' +
        '                   </textarea>' +
        '                   <input v-model="step.id" type="hidden" >' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteStep(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addStep">+ Добавить шаг</button>' +
        '              </div>' +
        '            </div>' +
        '</div>'
}