import fieldValidationEntity from "../../entity/fieldValidationEntity.js";

export default {
    name: 'SelectIngredientDetails',
    props: {
        recipe: {
            required: true,
            type: Object
        },
        selectedIngredientWrapper: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            validate: {
                count: fieldValidationEntity(),
                unit: fieldValidationEntity()
            }
        }
    },
    mounted() {

    },
    methods: {
        add() {

        }
    },
    template:
        '               <table class="table table-sm table-bordered table-striped table-hover">' +
        '                   <thead>' +
        '                       <tr>' +
        '                           <th>Калории</th>' +
        '                           <th>Жиры</th>' +
        '                           <th>Белки</th>' +
        '                           <th>Углеводы</th>' +
        '                       </tr>' +
        '                   </thead>' +
        '                   <tbody>' +
        '                       <tr style="cursor: pointer">' +
        '                           <td class="text-start">{{ selectedIngredientWrapper.selectedIngredient.calories }}</td>' +
        '                           <td class="text-start">{{ selectedIngredientWrapper.selectedIngredient.fat }}</td>' +
        '                           <td class="text-start">{{ selectedIngredientWrapper.selectedIngredient.protein }}</td>' +
        '                           <td class="text-start">{{ selectedIngredientWrapper.selectedIngredient.carbohydrates }}</td>' +
        '                       </tr>' +
        '                   </tbody>' +
        '               </table>' +

        '<form class="needs-validation mb-5" novalidate>' +
        '            <div class="form-floating mb-3">' +
        '              <input type="text" ' +
        '                   class="form-control"' +
        '                   :class="[ validate.count.valid ? \'is-valid\' : validate.count.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                   id="ingredientСount" ' +
        '                   placeholder="Количество"' +
        '                   v-model="selectedIngredientWrapper.ingredientToAdd.count"' +
        '                   @change="validate.count.clean()"' +
        '                   required aria-describedby="ingredientСount ingredientСountValidationFeedback">' +
        '              <label for="ingredientСount">Количество</label>' +
        '              <div v-show="validate.count.valid === false" class="invalid-feedback" ' +
        '                   id="ingredientСountValidationFeedback">' +
        '                   {{ validate.count.errorMsg }}' +
        '               </div>' +
        '            </div>' +
        '</form>'
}