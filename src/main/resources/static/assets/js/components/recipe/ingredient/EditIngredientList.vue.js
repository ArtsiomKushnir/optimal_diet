export default {
    name: 'EditIngredientList',
    props: {
        recipe: {
            required: true,
            type: Object
        }
    },
    data() {
        return {

        }
    },
    methods: {
        addIngredient() {
            this.recipe.ingredients.push({
                name: null,
                count: null,
                unit: null
            })
        },
        deleteIngredient(index) {
            this.recipe.ingredients.splice(index,1);
        },
    },
    template:
        '<div>' +
        '            <div class="row g-2 mb-3" v-for="(ingredient, index) in recipe.ingredients">' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.name" class="form-control form-control-sm" type="text" placeholder="Наименование" required>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.count" class="form-control form-control-sm" type="number" placeholder="Количество" required>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <input v-model="ingredient.unit" class="form-control form-control-sm" type="text" placeholder="Ед. измерения" required>' +
        '              </div>' +
        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteIngredient(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +
        '                   <button type="button" class="btn btn-success btn-sm" @click="addIngredient">+ Добавить ингредиент</button>' +
        '              </div>' +
        '            </div>' +
        '</div>'
}