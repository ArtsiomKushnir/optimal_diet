import NewIngredientAddModal from './NewIngredientAddModal.vue.js';

export default {
    name: 'TestEditIngredientList',
    components: {
        NewIngredientAddModal
    },
    props: {
        recipe: {
            required: true,
            type: Object
        }
    },
    data() {
        return {

        }
    },
    methods: {
        addIngredient() {
            this.recipe.ingredients.push({
                name: null,
                count: null,
                unit: null
            })
        },
        deleteIngredient(index) {
            this.recipe.ingredients.splice(index,1);
        },
        onModalInit() {
            this.$refs.NewIngredientAddModal.init();
        }
    },
    template:
        '<div>' +
        '            <div class="row g-2 mb-3" v-for="(ingredient, index) in recipe.ingredients">' +
        '              <div class="col-md">' +
        '                   <span>{{ index }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.name }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.count }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.unit }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.calories }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.protein }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.fat }}</span>' +
        '              </div>' +
        '              <div class="col-md">' +
        '                   <span>{{ ingredient.carbohydrates }}</span>' +
        '              </div>' +

        '              <div class="col-1">' +
        '                   <button type="button" class="btn btn-danger btn-sm" @click="deleteIngredient(index)">-</button>' +
        '              </div>' +
        '            </div>' +
        '            <div class="row g-2 mb-3">' +
        '              <div class="col-4">' +

        '                        <button class="btn-success btn-sm" ' +
        '                                type="button" ' +
        '                                data-bs-toggle="modal" ' +
        '                                @click="onModalInit()"' +
        '                                data-bs-target="#newIngredientAddModal">+ Добавить ингредиент</button>' +
        '                   <!--<button type="button" class="btn btn-success btn-sm" @click="addIngredient">+ Добавить ингредиент</button>-->' +
        '              </div>' +
        '            </div>' +
        '           <new-ingredient-add-modal ref="NewIngredientAddModal"></new-ingredient-add-modal>' +
        '' +
        '</div>'
}