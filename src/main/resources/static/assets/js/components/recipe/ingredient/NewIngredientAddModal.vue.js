import SelectIngredientDetails from "./SelectIngredientDetails.vue.js";

export default {
    name: 'NewIngredientAddModal',
    components: {
        SelectIngredientDetails
    },
    props: {
        recipe: {
            required: true,
                type: Object
        }
    },
    data() {
        return {
            ingredients: null,
            searchName: null,
            createNew: false,

            selectedIngredientWrapper: {
                selectedIngredient: null,
                ingredientToAdd: {
                    name: null,
                    count: null,
                    unit: null,
                    calories: null,
                    fat: null,
                    protein: null,
                    carbohydrates: null
                }
            },

            page: {
                content: [],
                sort: null,
                pageable: {
                    sort: null,
                    offset: 0,
                    pageNumber: 0,
                    pageSize: 4,
                    paged: null,
                    unpaged: null
                },
                last: null,
                first: null,
                totalPages: null,
                totalElements: null,
                number: null,
                size: null,
                numberOfElements: null,
                empty: null
            },
        }
    },
    mounted() {
        this.searchIngredient();
    },
    methods: {
        init() {
            this.searchIngredient();
        },
        onModalClose() {
            bootstrap.Modal.getInstance(document.getElementById('newIngredientAddModal')).hide();
            this.searchName = null;
            this.createNew = false;
            this.selectedIngredientWrapper.selectedIngredient = null;

            this.page = {
                content: [],
                sort: null,
                pageable: {
                    sort: null,
                    offset: 0,
                    pageNumber: 0,
                    pageSize: 4,
                    paged: null,
                    unpaged: null
                },
                last: null,
                first: null,
                totalPages: null,
                totalElements: null,
                number: null,
                size: null,
                numberOfElements: null,
                empty: null
            };
        },
        searchIngredient() {
            if (this.searchName) {
                this.getIngredients({
                    name: this.searchName,
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                })
            } else {
                this.getIngredients({
                    page: this.page.pageable.pageNumber,
                    size: this.page.pageable.pageSize
                })
            }
        },
        getIngredients(params) {
            axios
                .get('/api/ingredient',
                    {
                        params: params,
                        timeout: 3000
                    }
                )
                .then(response => {
                    this.page = response.data
                    this.ingredients = this.page.content;
                })
                .catch(error => console.log(error));
        },
        addIngredient() {
            this.$refs.SelectIngredientDetails.add();
        },
        createNewIngredient() {

        },
        backToSearchIngredients() {
            this.selectedIngredientWrapper.selectedIngredient = null;
        },
        selectIngredient(ingredient) {
            this.createNew = false;
            this.selectedIngredientWrapper.selectedIngredient = ingredient;
        },
        previousPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber - 1;
            this.getIngredients({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        nextPage() {
            this.page.pageable.pageNumber = this.page.pageable.pageNumber + 1;
            this.getIngredients({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        },
        toPage(n) {
            this.page.pageable.pageNumber = n - 1;
            this.getIngredients({
                page: this.page.pageable.pageNumber,
                size: this.page.pageable.pageSize
            })
        }
    },
    template:
        '<div class="modal fade"' +
        '       id="newIngredientAddModal"' +
        '       tabindex="-2"' +
        '       aria-hidden="true"' +
        '       data-bs-backdrop="static"' +
        '       data-bs-keyboard="false">' +
        '   <div class="modal-dialog modal-dialog-centered">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <h4 class="modal-title">Добавить ингредиент</h4>' +
        '               <button type="button" class="btn-close" @click="onModalClose" aria-label="Закрыть"></button>' +
        '           </div>' +
        '           <div class="modal-body">' +

        '               <template v-if="selectedIngredientWrapper.selectedIngredient">' +
        '                   <select-ingredient-details ref="SelectIngredientDetails" :selectedIngredientWrapper="selectedIngredientWrapper" ></select-ingredient-details>' +
        '               </template>' +

        '               <template v-else>' +
        '               <form class="needs-validation mb-5" novalidate>' +
        '                   <div class="row mb-2">' +
        '                       <div class="input-group mb-2">' +
        '                           <input type="search"' +
        '                                  v-model="searchName"' +
        '                                  class="form-control"' +
        '                                  placeholder="Поиск"' +
        '                                  aria-label="searchIngredient"' +
        '                                  aria-describedby="search-button">' +
        '                           <button class="btn btn-outline-secondary"' +
        '                                   type="button"' +
        '                                   id="search-button"' +
        '                                   @click="searchIngredient">Поиск</button>' +
        '                        </div>' +
        '                   </div>' +

        '               </form>' +
        '               <button type="button" class="btn btn-success btn-sm" @click="createNewIngredient">Создать новый ингредиент</button>' +

        '               <table class="table table-sm table-bordered table-striped table-hover">' +
        '                   <thead>' +
        '                       <tr>' +
        '                           <th>Название</th>' +
        '                           <th>Калории</th>' +
        '                           <th>Жиры</th>' +
        '                           <th>Белки</th>' +
        '                           <th>Углеводы</th>' +
        '                       </tr>' +
        '                   </thead>' +
        '                   <tbody>' +
        '                       <tr v-for="ingredient in ingredients" ' +
        '                           style="cursor: pointer"' +
        '                           @click="selectIngredient(ingredient)">' +
        '                           <td class="text-start">{{ ingredient.name }}</td>' +
        '                           <td class="text-start">{{ ingredient.calories }}</td>' +
        '                           <td class="text-start">{{ ingredient.fat }}</td>' +
        '                           <td class="text-start">{{ ingredient.protein }}</td>' +
        '                           <td class="text-start">{{ ingredient.carbohydrates }}</td>' +
        '                       </tr>' +
        '                   </tbody>' +
        '               </table>' +

        '                           <nav v-if="!page.empty && (!page.last || !page.first)">' +
        '                               <ul class="pagination justify-content-center pagination-sm">' +

        '                                   <template v-if="page.first">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link" tabindex="-1" aria-disabled="true">&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="previousPage" >&laquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                                   <template v-for="n in page.totalPages">' +
        '                                       <template v-if="n === page.number + 1">' +
        '                                           <li class="page-item active" aria-current="page">' +
        '                                               <span class="page-link">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                       <template v-else>' +
        '                                           <li class="page-item">' +
        '                                               <span class="page-link" @click="toPage(n)">{{ n }}</span>' +
        '                                           </li>' +
        '                                       </template>' +
        '                                   </template>' +

        '                                   <template v-if="page.last">' +
        '                                       <li class="page-item disabled">' +
        '                                           <span class="page-link">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +
        '                                   <template v-else>' +
        '                                       <li class="page-item">' +
        '                                           <span class="page-link" @click="nextPage">&raquo;</span>' +
        '                                       </li>' +
        '                                   </template>' +

        '                               </ul>' +
        '                           </nav>' +

        '               </template>' +


        '           </div>' +
        '           <div class="modal-footer">' +
        '               <button type="button" ' +
        '                       v-if="selectedIngredientWrapper.selectedIngredient"' +
        '                       class="btn btn-primary" ' +
        '                       @click="addIngredient">Добавить ингредиент</button>' +
        '               <button type="button" class="btn btn-secondary" @click="onModalClose" v-if="!selectedIngredientWrapper.selectedIngredient">Отмена</button>' +
        '               <button type="button" class="btn btn-secondary" @click="backToSearchIngredients" v-if="selectedIngredientWrapper.selectedIngredient">Назад</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>'
}