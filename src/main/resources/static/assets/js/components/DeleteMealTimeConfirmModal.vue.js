export default {
    name: 'DeleteMealTimeConfirmModal',
    props: {
        user: {
            required: true,
            type: Object
        },
        plan: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            mealTime: {}
        }
    },
    methods: {
        init(mealTime) {
            this.mealTime = mealTime;
        },
        onModalClose() {
            this.mealTime = {};
        },
        confirmDelete() {
            axios
                .delete(
                    '/api/mealTime?dayOfWeek=' +  this.mealTime.dayOfWeek +
                    '&weekDirectionId=' + this.mealTime.weekDirectionId +
                    '&orderId=' + this.mealTime.orderId
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.user.id = response.data.user.id;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                    this.onModalClose();
                    bootstrap.Modal.getInstance(document.getElementById('deleteMealTimeConfirmModal')).hide();
                })
                .catch(error => console.log(error.response))
        }
    },
    template:
        '<div class="modal fade" ' +
        'id="deleteMealTimeConfirmModal" ' +
        'data-bs-backdrop="static" ' +
        'data-bs-keyboard="false" ' +
        'tabindex="-1" ' +
        'aria-labelledby="deleteMealTimeConfirmModalLabel" aria-hidden="true">' +
        '  <div class="modal-dialog">' +
        '    <div class="modal-content">' +
        '      <div class="modal-header">' +
        '        <h5 class="modal-title" id="deleteMealTimeConfirmModalLabel">Подтверждение удаления времени приема пищи для: {{ mealTime.name }}, Недели - {{ mealTime.weekDirectionId }}</h5>' +
        '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close" @click="onModalClose"></button>' +
        '      </div>' +
        '      <div class="modal-body">' +
        '        Время приема пищи ({{ mealTime.name }}) содержит данные о рецептах.\n' +
        '        Вы уверены что ходите удалить прием пищи?' +
        '      </div>' +
        '      <div class="modal-footer">' +
        '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" @click="onModalClose">Отмена</button>' +
        '        <button type="button" class="btn btn-warning" @click="confirmDelete">Удалить</button>' +
        '      </div>\n' +
        '    </div>\n' +
        '  </div>\n' +
        '</div>'
}