import fieldValidationEntity from './entity/fieldValidationEntity.js'

export default {
    name: 'UserLoginModal',
    props: {
        user: {
            required: true,
            type: Object
        },
        plan: {
            required: true,
            type: Object
        }
    },
    data() {
        return {
            userDto: {
                email: null,
                password: null,
                confirmPassword: null
            },
            validation: {
                email: fieldValidationEntity(),
                password: fieldValidationEntity(),
                confirmPassword: fieldValidationEntity()
            }
        }
    },
    methods: {
        onModalClose() {
            this.userDto = {
                email: null,
                password: null,
                confirmPassword: null
            };
        },
        login() {
            axios
                .post(
                    '/api/user/login',
                    this.userDto
                ).then(response => {
                    this.user.email = response.data.user.email;
                    this.user.id = response.data.user.id;
                    this.user.groups = response.data.user.groups;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                    bootstrap.Modal.getInstance(document.getElementById('userLoginModal')).hide();
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.email) {
                            this.validation.email.errorMsg = error.response.data.email;
                            this.validation.email.validate();
                        }
                        if (error.response.data.password) {
                            this.validation.password.errorMsg = error.response.data.password;
                            this.validation.password.validate();
                        }
                    }
                });
        },
        register() {
            axios
                .post(
                    '/api/user/register',
                    this.userDto
                ).then(response => {
                    this.user.email = response.data.email;
                    this.user.id = response.data.id;
                    this.user.groups = response.data.groups;
                    bootstrap.Modal.getInstance(document.getElementById('userRegisterModal')).hide();
                })
                .catch(error => {
                    if (error.response.status === 400) {
                        if (error.response.data.email) {
                            this.validation.email.errorMsg = error.response.data.email;
                            this.validation.email.validate();
                        }
                        if (error.response.data.password) {
                            this.validation.password.errorMsg = error.response.data.password;
                            this.validation.password.validate();
                        }
                        if (error.response.data.confirmPassword) {
                            this.validation.confirmPassword.errorMsg = error.response.data.confirmPassword;
                            this.validation.confirmPassword.validate();
                        }
                    }
                });
        }
    },
    template:
        '<div>' +
        '<div class="modal fade"' +
        '       id="userLoginModal"' +
        '       tabindex="-1"' +
        '       aria-hidden="true"' +
        '       data-bs-backdrop="static"' +
        '       data-bs-keyboard="false">' +
        '   <div class="modal-dialog modal-dialog-centered modal-login">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <h4 class="modal-title">Войти</h4>' +
        '               <button type="button" class="btn-close" data-bs-dismiss="modal" @click="onModalClose" aria-label="Закрыть"></button>' +
        '           </div>' +
        '           <div class="modal-body">' +
        '               <form class="needs-validation" novalidate>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.email.valid ? \'is-valid\' : validation.email.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="emailLogin" ' +
        '                               placeholder="Адрес эл. почты" ' +
        '                               @change="validation.email.clean()" ' +
        '                               v-model="userDto.email"' +
        '                               required aria-describedby="emailLoginValidationFeedback">' +
        '                       <label for="emailLogin">Адрес эл. почты</label>' +
        '                       <div v-if="validation.email.valid === false" class="invalid-feedback" ' +
        '                               id="emailLoginValidationFeedback">' +
        '                               {{ validation.email.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="password" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.password.valid ? \'is-valid\' : validation.password.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="passwordLogin" ' +
        '                               placeholder="Пароль" ' +
        '                               @change="validation.password.clean()" ' +
        '                               v-model="userDto.password"' +
        '                               required aria-describedby="passwordLoginValidationFeedback">' +
        '                       <label for="passwordLogin">Пароль</label>' +
        '                       <div v-if="validation.password.valid === false" class="invalid-feedback" ' +
        '                               id="passwordLoginValidationFeedback">' +
        '                               {{ validation.password.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '               </form>' +
        '           </div>' +
        '           <div class="modal-footer" style="justify-content: center;">' +
        '               <button class="btn btn-primary" @click="login">Войти</button>' +
        '               <button class="btn btn-primary" ' +
        '                       data-bs-target="#userRegisterModal" ' +
        '                       data-bs-toggle="modal" ' +
        '                       data-bs-dismiss="modal">Зарегистрироваться</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +

        '<div class="modal fade"' +
        '       id="userRegisterModal"' +
        '       tabindex="-1"' +
        '       aria-hidden="true"' +
        '       data-bs-backdrop="static"' +
        '       data-bs-keyboard="false">' +
        '   <div class="modal-dialog modal-dialog-centered modal-login">' +
        '       <div class="modal-content">' +
        '           <div class="modal-header">' +
        '               <h4 class="modal-title">Регистрация</h4>' +
        '               <button type="button" class="btn-close" data-bs-dismiss="modal" @click="onModalClose" aria-label="Закрыть"></button>' +
        '           </div>' +
        '           <div class="modal-body">' +
        '               <form class="needs-validation" novalidate>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="text" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.email.valid ? \'is-valid\' : validation.email.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="emailRegister" ' +
        '                               placeholder="Адрес эл. почты" ' +
        '                               @change="validation.email.clean()" ' +
        '                               v-model="userDto.email"' +
        '                               required aria-describedby="emailRegisterValidationFeedback">' +
        '                       <label for="emailRegister">Адрес эл. почты</label>' +
        '                       <div v-if="validation.email.valid === false" class="invalid-feedback" ' +
        '                               id="emailRegisterValidationFeedback">' +
        '                               {{ validation.email.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="password" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.password.valid ? \'is-valid\' : validation.password.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="passwordRegister" ' +
        '                               placeholder="Пароль" ' +
        '                               @change="validation.password.clean()" ' +
        '                               v-model="userDto.password"' +
        '                               required aria-describedby="passwordRegisterValidationFeedback">' +
        '                       <label for="passwordRegister">Пароль</label>' +
        '                       <div v-if="validation.password.valid === false" class="invalid-feedback" ' +
        '                               id="passwordRegisterValidationFeedback">' +
        '                               {{ validation.password.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '                   <div class="form-floating mb-3">' +
        '                       <input type="password" ' +
        '                               class="form-control" ' +
        '                               :class="[ validation.confirmPassword.valid ? \'is-valid\' : validation.confirmPassword.valid === false?  \'is-invalid\' : \'\' ]"' +
        '                               id="confirmPasswordRegister" ' +
        '                               placeholder="Пароль" ' +
        '                               @change="validation.confirmPassword.clean()" ' +
        '                               v-model="userDto.confirmPassword"' +
        '                               required aria-describedby="confirmPasswordRegisterValidationFeedback">' +
        '                       <label for="confirmPasswordRegister">Пароль</label>' +
        '                       <div v-if="validation.confirmPassword.valid === false" class="invalid-feedback" ' +
        '                               id="confirmPasswordRegisterValidationFeedback">' +
        '                               {{ validation.confirmPassword.errorMsg }}' +
        '                       </div>' +
        '                   </div>' +
        '               </form>' +
        '           </div>' +
        '           <div class="modal-footer" style="justify-content: center;">' +
        '               <button class="btn btn-primary" @click="register">Зарегистрироваться</button>' +
        '               <button class="btn btn-primary" ' +
        '                       data-bs-target="#userLoginModal" ' +
        '                       data-bs-toggle="modal" ' +
        '                       data-bs-dismiss="modal">Войти</button>' +
        '           </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '</div>'
}