import UserLoginModal from "./components/UserLoginModal.vue.js";
import UserNavigation from "./components/UserNavigation.vue.js";
import AddRecipeToDietModal from './components/AddRecipeToDietModal.vue.js';
import DeleteWeekConfirmModal from './components/DeleteWeekConfirmModal.vue.js';
import AddMealTimeModal from './components/AddMealTimeModal.vue.js';
import DeleteMealTimeConfirmModal from './components/DeleteMealTimeConfirmModal.vue.js';
import ConfigurationModal from './components/admin/ConfigurationModal.vue.js';

import defaultMealTimes from './components/entity/defaultMealTimes.js'
import defaultCalculationData from './components/entity/defaultCalculationData.js'

const app = new Vue({
    el: '#app',
    data() {
        return {
            days: ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье'],
            user: {
                id: null,
                email: null,
                groups: []
            },
            plan: {
                formsData: {
                    personData: {
                        gender: null,
                        age: null,
                        height: null,
                        weight: null,
                        activityLevel: null
                    },
                    dietAim: null,
                    nutrientsAim: {
                        proteinCoefficient: null,
                        fatCoefficient: null,
                        carbohydratesCoefficient: null
                    }
                },
                calculationResults: {
                    base: defaultCalculationData(),
                    corrected: defaultCalculationData(),
                    actualTotal: defaultCalculationData(),
                    outOfAim: false
                },
                weeks: {
                    1: {
                        outOfAim: false,
                        directionId: 1,
                        days: {
                            1: {
                                dayOfWeek: 1,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            2: {
                                dayOfWeek: 2,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            3: {
                                dayOfWeek: 3,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            4: {
                                dayOfWeek: 4,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            5: {
                                dayOfWeek: 5,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            6: {
                                dayOfWeek: 6,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                            7: {
                                dayOfWeek: 7,
                                calculation: defaultCalculationData(),
                                mealTimes: defaultMealTimes()
                            },
                        }
                    }
                }
            },
            selected: {
                week: null,
                day: null,
                mealTime: null
            },
            isOutOfAim: false
        }
    },
    mounted() {
        this.getPlan();
    },
    methods: {
        getPlan() {
            axios
                .get(
                    '/api/calculator'
                )
                .then(response => {
                    this.plan = response.data
                })
                .catch(error => console.log(error.response))

        },
        calculate() {
            axios
                .post(
                    '/api/calculator',
                    this.plan
                )
                .then(response => {
                    this.plan = response.data
                    console.log(this.plan)
                })
                .catch(error => console.log(error.response))

        },
        openAddRecipeToMealTime(week, day, mealTime) {
            this.selected.week = week;
            this.selected.day = day;
            this.selected.mealTime = mealTime;
            this.$refs.AddRecipeToDietModal.init();
        },
        deleteRecipeFromMealTime(recipe, mealTime) {
            axios
                .delete(
                    '/api/plan/mealTime/' + mealTime.orderId +
                    '/recipe/' + recipe.id +
                    '?weekDirectionId=' + mealTime.weekDirectionId +
                    '&dayOfWeek=' + mealTime.dayOfWeek
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                })
                .catch(error => console.log(error.response))

        },
        addWeek() {
            axios
                .post(
                    '/api/plan/week'
                )
                .then(response => {
                    this.user.email = response.data.user.email;
                    this.plan.formsData = response.data.plan.formsData;
                    this.plan.calculationResults = response.data.plan.calculationResults;
                    this.plan.weeks = response.data.plan.weeks;
                })
                .catch(error => console.log(error.response))

        },
        deleteWeek(week) {
            let hasData = false;

            Object.entries(week.days).forEach((day) => {
                Object.entries(day[1]['mealTimes']).forEach((mealTime) => {
                    if (mealTime[1]['recipes'].length > 0) {
                        hasData = true;
                    }
                })
            })

            if (hasData) {
                this.openDeleteWeekConfirmModal(week.directionId);
            } else {
                axios
                    .delete(
                        '/api/plan/week?directionId=' + week.directionId
                    )
                    .then(response => {
                        this.user.email = response.data.user.email;
                        this.plan.formsData = response.data.plan.formsData;
                        this.plan.calculationResults = response.data.plan.calculationResults;
                        this.plan.weeks = response.data.plan.weeks;
                    })
                    .catch(error => console.log(error.response))
           }

        },
        openDeleteWeekConfirmModal(directionId) {
            this.$refs.DeleteWeekConfirmModal.init(directionId);
            new bootstrap.Modal(document.getElementById('deleteWeekConfirmModal'), {}).show();
        },
        addMealTime(dayOfWeek, dayName, weekDirectionId) {
            this.$refs.AddMealTimeModal.init(dayOfWeek, dayName, weekDirectionId);
        },
        deleteMealTime(mealTime) {
            if (Object.keys(mealTime.recipes).length > 0) {
                this.$refs.DeleteMealTimeConfirmModal.init(mealTime);
                new bootstrap.Modal(document.getElementById('deleteMealTimeConfirmModal'), {}).show();
            } else {
                axios
                    .delete(
                        '/api/mealTime?dayOfWeek=' +  mealTime.dayOfWeek +
                        '&weekDirectionId=' + mealTime.weekDirectionId +
                        '&orderId=' + mealTime.orderId
                    )
                    .then(response => {
                        this.user.email = response.data.user.email;
                        this.plan.formsData = response.data.plan.formsData;
                        this.plan.calculationResults = response.data.plan.calculationResults;
                        this.plan.weeks = response.data.plan.weeks;
                    })
                    .catch(error => console.log(error.response))
            }

        }
    },
    components: {
        UserLoginModal,
        UserNavigation,
        AddRecipeToDietModal,
        DeleteWeekConfirmModal,
        AddMealTimeModal,
        DeleteMealTimeConfirmModal,
        ConfigurationModal
    },
});