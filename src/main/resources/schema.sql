CREATE TABLE IF NOT EXISTS users (
    id                  BIGINT          AUTO_INCREMENT PRIMARY KEY,
    email               VARCHAR(100)    NOT NULL UNIQUE,
    password            VARCHAR(100)    NOT NULL,
    enabled             BOOLEAN         NOT NULL,
    groups              VARCHAR(100)    NOT NULL DEFAULT 'USER'
);
CREATE INDEX IF NOT EXISTS ix_email_password ON users (email, password);

CREATE TABLE IF NOT EXISTS user_forms_data (
    user_id                           BIGINT          NOT NULL UNIQUE,
    gender                            VARCHAR(100)    ,
    age                               SMALLINT        ,
    height                            SMALLINT        ,
    weight                            SMALLINT        ,
    activity_level                    VARCHAR(100)    ,
    diet_aim                          VARCHAR(100)    ,
    selected_protein_coefficient      DOUBLE,
    selected_fat_coefficient          DOUBLE,
    FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE IF NOT EXISTS category (
    id                  INT                 AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS dish (
    id                  INT                 AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL,
    category_id         INT                 NOT NULL,
    UNIQUE (name, category_id),
    FOREIGN KEY (category_id) REFERENCES category(id)
);

CREATE TABLE IF NOT EXISTS recipe (
    id                  BIGINT              AUTO_INCREMENT PRIMARY KEY,
    user_id             BIGINT              NOT NULL,
    name                VARCHAR(150)        NOT NULL,
    description         TEXT                ,
    cooking_time_min    SMALLINT            NOT NULL,
    number_of_servings  TINYINT             NOT NULL DEFAULT 1,
    dish_id             BIGINT              NOT NULL,
    FOREIGN KEY (dish_id) REFERENCES dish(id)
);

CREATE TABLE IF NOT EXISTS units (
    id                  BIGINT              AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(250)        NOT NULL,
    short_name          VARCHAR(50)         NOT NULL,
    type                INT                 NOT NULL,
    coefficient         DOUBLE              NOT NULL,
    groups              TEXT
);

CREATE TABLE IF NOT EXISTS recipe_ingredients (
    recipe_id           BIGINT              NOT NULL,
    name                VARCHAR(150)        NOT NULL,
    count               DECIMAL             NOT NULL,
    unit                VARCHAR(150)        NOT NULL,
    unit_coefficient    DOUBLE              NOT NULL,
    calories            DECIMAL             ,
    fat                 DECIMAL             ,
    protein             DECIMAL             ,
    carbohydrates       DECIMAL             ,
    PRIMARY KEY (recipe_id, name),
    FOREIGN KEY (recipe_id) REFERENCES recipe(id)
);

CREATE TABLE IF NOT EXISTS ingredients (
    id                  BIGINT              AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(250)        NOT NULL UNIQUE,
    calories            DECIMAL             ,
    fat                 DECIMAL             ,
    protein             DECIMAL             ,
    carbohydrates       DECIMAL
);

CREATE TABLE IF NOT EXISTS recipe_steps (
    recipe_id           BIGINT              NOT NULL,
    step_id             BIGINT              NOT NULL,
    step_description    TEXT                NOT NULL,
    PRIMARY KEY (recipe_id, step_id),
    FOREIGN KEY (recipe_id) REFERENCES recipe(id)
);

CREATE TABLE IF NOT EXISTS user_diet_plan (
    id                  BIGINT              AUTO_INCREMENT PRIMARY KEY,
    name                VARCHAR(150)        NOT NULL,
    user_id             BIGINT              NOT NULL,
    week_id             TINYINT             NOT NULL,
    day_of_week         TINYINT             NOT NULL,
    scheduled_time      TIME                NOT NULL,
    order_id            BIGINT              NOT NULL,
    ration_volume       DOUBLE              ,
    is_current          BOOLEAN             NOT NULL,
    recipe_ids          TEXT
);
CREATE UNIQUE INDEX IF NOT EXISTS uix_user_diet_plan ON user_diet_plan (user_id, week_id, day_of_week, order_id);